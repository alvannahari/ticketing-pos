<?php

namespace Database\Seeders;

use App\Models\TicketSale;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketSaleSeeder extends Seeder {

    public function run(): void {
        for ($i=1; $i <= 8; $i++) { 
            $date = Carbon::now()->startOfWeek()->addDays(random_int(0,6));
            $sale = TicketSale::create([
                'user_id' => random_int(3,4),
                'cust_name' => 'Pengunjung Ke-'.$i,
                'total_price' => 75000,
                'discount' => 0,
                'final_amount' => 75000,
                'paid_amount' => 100000,
                'change' => 25000,
                'type_payment_id' => random_int(1,3),
                'created_at' => $date,
                'updated_at' => $date
            ]);

            DB::table('ticket_sale_items')->insert([
                'sale_id' => $sale->id,
                'ticket_type_id' => 2,
                'name' => 'Tiket Domestik',
                'price' => 25000,
                'qty' => 1,
                'created_at' => $date,
                'updated_at' => $date
            ]);

            DB::table('ticket_sale_items')->insert([
                'sale_id' => $sale->id,
                'ticket_type_id' => 3,
                'name' => 'Tiket Nasional',
                'price' => 50000,
                'qty' => 1,
                'created_at' => $date,
                'updated_at' => $date
            ]);
        }
    }
}
