<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypePaymentSeeder extends Seeder {
    
    public function run(): void {
        DB::table('type_payments')->insert([
            'name' => 'Tunai',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('type_payments')->insert([
            'name' => 'QRIS',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('type_payments')->insert([
            'name' => 'EDC / Non-Tunai',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
