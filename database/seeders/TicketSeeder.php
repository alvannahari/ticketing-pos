<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void {
        DB::table('ticket_types')->insert([
            'name' => 'Tiket Gratis',
            'price' => 0,
            'image' => 'tiket_gratis.jpg',
            'inventory' => 250,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('ticket_types')->insert([
            'name' => 'Tiket Domestik',
            'price' => 25000,
            'image' => 'tiket_domestik.jpg',
            'inventory' => 242,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('ticket_types')->insert([
            'name' => 'Tiket Nasional',
            'price' => 50000,
            'image' => 'tiket_nasional.jpg',
            'inventory' => 242,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('ticket_types')->insert([
            'name' => 'Tiket Internasional',
            'price' => 100000,
            'image' => 'tiket_internasional.jpg',
            'inventory' => 250,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        for ($i=1; $i <= 1000; $i++) { 

            // $barcode = substr(str_shuffle('0123456789'),1,random_int(5,8));
            // $length = strlen($barcode);
            switch ($i) {
                case $i <= 250: $type = 1;$length = 5;break;
                case $i <= 500: $type = 2;$length = 6;break;
                case $i <= 750: $type = 3;$length = 7;break;
                default: $type = 4;$length = 7;break;
            }
            $barcode = substr(str_shuffle('01234567890123456789'),1,$length);

            DB::table('tickets')->insert([
                'type_id' => $type,
                'barcode' => $barcode,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
