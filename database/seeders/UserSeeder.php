<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder {
    
    public function run(): void {
        $owner = User::create([
            'role_id' => 1,
            'username' => 'superadmin-museum',
            'name' => 'Akun Super Admin',
            "password" => bcrypt('superadmin-museum'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $owner->assignRole('owner');

        // Owner
        $owner = User::create([
            'role_id' => 1,
            'username' => 'manajerpetugas01',
            'name' => 'Manajer Petugas / Owner',
            "password" => bcrypt('manajerpetugas01'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $owner->assignRole('owner');

        // Petugas Tiket
        $ticket = User::create([
            'role_id' => 2,
            'username' => 'petugas-loket01',
            'name' => 'Petugas Loket 01',
            "password" => bcrypt('Mu53umSBY*ANI'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $ticket->assignRole('ticket');
        $ticket = User::create([
            'role_id' => 2,
            'username' => 'petugas-loket02',
            'name' => 'Petugas Loket 02',
            "password" => bcrypt('Mu53umSBY*ANI'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $ticket->assignRole('ticket');
        $gate = User::create([
            'role_id' => 2,
            'username' => 'petugas-gate01',
            'name' => 'Petugas Gerbang 01',
            "password" => bcrypt('Mu53umSBY*ANI'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $gate->assignRole('ticket');

        // Petugas Store
        $pos = User::create([
            'role_id' => 3,
            'username' => 'petugas-store01',
            'name' => 'Petugas Store 01',
            "password" => bcrypt('petugas-store01'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $pos->assignRole('pos');
        $pos = User::create([
            'role_id' => 3,
            'username' => 'petugas-store02',
            'name' => 'Petugas Store 02',
            "password" => bcrypt('petugas-store02'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $pos->assignRole('pos');
    }
}
