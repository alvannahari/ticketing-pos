<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder{

    public function run(): void {

        $owner = \Spatie\Permission\Models\Role::create(['name' => 'owner']);
        $roleTicket = \Spatie\Permission\Models\Role::create(['name' => 'ticket']);
        $rolePos = \Spatie\Permission\Models\Role::create(['name' => 'pos']);

        $permissions = [
            'import-tickets',
            'view-tickets',
            'delete-tickets',
            'view-sale-tickets',
            'view-products',
            'create-products',
            'edit-products',
            'delete-products',
            'view-sale-products',
            'create-sale-products',
            'delete-sale-products',
            'synchronize-sale-products',
            'view-users',
            'create-users',
            'edit-users',
            'delete-users',
            'view-activities',
        ];

        foreach($permissions as $permissionName) {
            $permission = \Spatie\Permission\Models\Permission::create(['name' => $permissionName]);
            $owner->givePermissionTo($permission);
        
            if(str_contains($permissionName, 'ticket')) {
                $roleTicket->givePermissionTo($permission);
            } elseif (str_contains($permissionName, 'product')) {
                $rolePos->givePermissionTo($permission);
            }
        }
    }
}
