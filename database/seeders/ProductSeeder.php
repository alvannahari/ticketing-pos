<?php

namespace Database\Seeders;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder {

    public function run(): void {
        $data = [
            ['Gantungan Kunci', 5000],
            ['Pulpen', 4000],
            ['Pembatas Buku', 5000],
            ['Pensil', 2000],
            ['Magnet Kulkas', 5000],
            ['Wayang Kulit', 50000],
            ['Wayang Golek', 60000],
            ['Dompet', 25000],
            ['Kaos', 30000],
            ['Peci', 30000],
            ['Patung Kecil', 40000],
            ['Patung Besar', 60000],
            ['Snack', 5000],
            ['Foto', 5000],
            ['Kalung', 7000],
            ['Gelang', 4000],
            ['Boneka', 15000],
            ['Sandal', 15000],
        ];

        foreach ($data as $key => $value) {
            Product::create([
                'name' => $value[0],
                // 'code' => substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,12),
                'code' => substr(str_shuffle('0123456789'),1,16),
                'inventory' => random_int(1,30),
                'capital_price' => $value[1] * 0.5,
                'price' => $value[1],
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
