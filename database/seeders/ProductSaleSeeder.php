<?php

namespace Database\Seeders;

use App\Models\ProductSale;
use App\Models\ProductSaleItem;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ProductSaleSeeder extends Seeder {

    public function run(): void {
        for ($i=1; $i <= 8; $i++) { 
            $date = Carbon::now()->startOfWeek()->addDays(random_int(0,6));

            $sale = ProductSale::create([
                'user_id' => 2,
                'type_payment_id' => random_int(1,2),
                'cust_name' => 'Pembeli Ke-'.$i,
                'total_price' => 200000,
                'discount' => 10000,
                'final_amount' => 190000,
                'paid_amount' => 200000,
                'change' => 10000,
                'created_at' => $date,
                'updated_at' => $date
            ]);

            ProductSaleItem::create([
                'sale_id' => $sale->id,
                'product_id' => ($i * 2) - 1,
                'name' => 'Barang 1',
                'qty' => random_int(1,10),
                'code' => "ITEM-".substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,7),
                'capital_price' => 80000,
                'price' => 100000,
                'profit' => 20000,
                'created_at' => $date,
                'updated_at' => $date
            ]);
            ProductSaleItem::create([
                'sale_id' => $sale->id,
                'product_id' => $i * 2,
                'name' => 'Barang 1',
                'qty' => random_int(1,10),
                'code' => "ITEM-".substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'),1,7),
                'capital_price' => 80000,
                'price' => 100000,
                'profit' => 20000,
                'created_at' => $date,
                'updated_at' => $date
            ]);
        }
    }
}
