<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    public function up(): void {
        Schema::create('product_sales', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignId('user_id');
            $table->foreignId('type_payment_id');
            $table->string('cust_name')->nullable();
            $table->unsignedInteger('total_price');
            $table->unsignedInteger('discount')->nullable();
            $table->unsignedInteger('final_amount');
            $table->unsignedInteger('paid_amount');
            $table->unsignedInteger('change')->default(0);
            $table->string('note')->nullable();
            $table->tinyInteger('is_completed')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_sales');
    }
};
