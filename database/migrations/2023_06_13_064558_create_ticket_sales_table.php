<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ticket_sales', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->foreignId('user_id');
            $table->foreignId('type_payment_id');
            $table->string('cust_name')->nullable();
            $table->unsignedBigInteger('total_price');
            $table->unsignedBigInteger('discount')->nullable();
            $table->unsignedBigInteger('final_amount');
            $table->unsignedBigInteger('paid_amount');
            $table->unsignedBigInteger('change');
            $table->string('note')->nullable();
            $table->tinyInteger('is_completed')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ticket_sales');
    }
};
