<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    public function up(): void {
        Schema::create('product_sale_items', function (Blueprint $table) {
            $table->id();
            $table->string('sale_id');
            $table->string('product_id');
            $table->string('name');
            $table->string('code')->nullable();
            $table->unsignedInteger('qty');
            $table->unsignedInteger('capital_price');
            $table->unsignedInteger('price');
            $table->unsignedInteger('profit');
            $table->string('desc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_sale_items');
    }
};
