<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ticket_sale_items', function (Blueprint $table) {
            $table->id();
            $table->string('sale_id');
            $table->foreignId('ticket_type_id');
            $table->string('name');
            $table->unsignedInteger('price');
            $table->unsignedInteger('qty');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ticket_sale_items');
    }
};
