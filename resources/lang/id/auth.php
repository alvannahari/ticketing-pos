<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Email atau password tidak cocok.',
    'password' => 'Password yang digunakan tidak cock.',
    'throttle' => 'Terlalu banyak percobaan. Coba lagi dalam :seconds detik.',

];
