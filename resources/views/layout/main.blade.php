<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>{{ config('app.name') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @stack('prepend-style')
    @include('layout.style')
    @stack('addons-style')
</head>

<body>
    <div class="loader"></div>
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        <nav class="navbar navbar-expand-lg main-navbar sticky">
            <div class="form-inline mr-auto">
                <ul class="navbar-nav">
                    <li>
                        <a href="#" data-toggle="sidebar" class="nav-link nav-link-lg collapse-btn"> 
                            <i data-feather="align-justify"></i>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="nav-link nav-link-lg fullscreen-btn">
                            <i data-feather="maximize"></i>
                        </a>
                    </li>
                    <li style="display: flex;align-items: center;">
                        <div class="search-element">
                            <h5 class="mb-0 mt-1 ml-2 text-secondary" style="font-size:1.1rem">
                                @yield('title')
                            </h5>
                        </div>
                    </li>
                </ul>
            </div>
            <ul class="navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                        <span style="font-size:16px" class="mr-2">Hello, {{ auth()->user()->name }}</span> 
                        <img alt="image" src="{{ asset('assets/img/user.png') }}" class="user-img-radious-style"> 
                        <span class="d-sm-none d-lg-inline-block"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right pullDown">
                        <div class="dropdown-title">Hello {{ auth()->user()->getRoleNames()->first() }}</div>
                        {{-- <a href="#" class="dropdown-item has-icon" onclick="changePassword({{ auth()->id() }})"> 
                            <i class="far fa-user"></i> Ganti Password
                        </a>  --}}
                        @can('view-activities')
                        <a href="{{ route('log-activities.index') }}" class="dropdown-item has-icon"> 
                            <i class="fas fa-bolt"></i> Aktifitas
                        </a> 
                        @endcan
                        <div class="dropdown-divider"></div>
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button class="dropdown-item has-icon text-danger" style="font-weight: 500;line-height: 1.2;font-size: 13px;" type="submit"> 
                                <i class="fas fa-sign-out-alt"></i>
                                Logout
                            </button>
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="main-sidebar sidebar-style-2">
            <aside id="sidebar-wrapper">
                <div class="sidebar-brand">
                <a href="{{ route('dashboard') }}" style="display: inline-flex;padding-top: 12px;padding-left: 6px;"> 
                    <img alt="image" src="{{ asset('assets/img/logo_main.png') }}" class="header-logo" style="height: 55px;" /> 
                    <div style="text-align: left;padding-top: 4px;padding-left: 8px;" id="name-apps">
                        <p style="text-transform: none;font-weight: 500;font-size: 13px;letter-spacing: 0px;margin-bottom: 0px;">Sistem Informasi</p>
                        <p style="text-transform: none;font-weight: 700;font-size: 13px;letter-spacing: 0px;margin-bottom: 0px;line-height: 10px;">{{ config('app.name') }}</p>
                    </div>
                </a>
                </div>
                <ul class="sidebar-menu">
                    @include('layout.sidebar')
                </ul>
            </aside>
        </div>
        <!-- Main Content -->
        <div class="main-content">
            @yield('content')
        </div>
        <div id="notifications-state">
            @if (Session::get('status'))
                <span data-message="{{ Session::get('message') }}" data-status="{{ Session::get('status') }}"></span>
            @endif
        </div>
        <footer class="main-footer">
            <div class="footer-left">
                <a href="https://krakatio.com">Krakatio</a></a>
            </div>
            <div class="footer-right"></div>
        </footer>
    </div>

    @stack('prepend-script')
    @include('layout.script')
    <script>
        function clearError(form) {
            // if($(that).hasClass('shopping')){
            //     $(that).removeClass('shopping');
            //     alert( $(that).attr('class'));//btn btn-warning
            // }
            $('#'+form).find('.is-invalid').next().remove();
            $('#'+form).find('.is-invalid').removeClass('is-invalid');
            // .removeClass('is-invalid').next().remove();
            // $('#'+form+' textarea').removeClass('is-invalid').next().remove();
        }
    </script>
    @stack('addons-script')
</body>
<!-- index.html  21 Nov 2019 03:47:04 GMT -->
</html>