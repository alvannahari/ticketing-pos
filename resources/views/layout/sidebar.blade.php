<li class="menu-header">Menu Navigasi</li>

@role('owner')
<li class="dropdown {{ request()->is('dashboard-ticket') ? 'active' : '' }}">
    <a href="{{ route('dashboard.ticket') }}" class="nav-link"><i class="fas fa-desktop"></i><span>Dashboard Tiket</span></a>
</li>
<li class="dropdown {{ request()->is('dashboard-store') ? 'active' : '' }}">
    <a href="{{ route('dashboard.store') }}" class="nav-link"><i class="fas fa-desktop"></i><span>Dashboard Store Produk</span></a>
</li>
@endrole
@hasanyrole('pos|ticket')
<li class="dropdown {{ request()->is('dashboard') ? 'active' : '' }}">
    <a href="{{ route('dashboard') }}" class="nav-link"><i class="fas fa-desktop"></i><span>Dashboard</span></a>
</li>
@endhasanyrole  

@can('view-tickets')
<li class="menu-header">Data Menu Tiket</li> {{-- Menu Tiket --}}

<li class="dropdown {{ request()->is('tickets*') ? 'active' : '' }}">
    <a href="#" class="menu-toggle nav-link has-dropdown" style="padding-left: 16px;"><i class="fas fa-ticket-alt"></i><span>Tiket</span></a>
    <ul class="dropdown-menu">
        <li class="{{ request()->is('tickets-valid') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('ticket.valid') }}">Valid</a>
        </li>
        <li class="{{ request()->is('tickets-invalid') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('ticket.invalid') }}">Invalid</a>
        </li>
    </ul>
</li>
@endcan

@can('view-sale-tickets')
<li class="dropdown {{ request()->is('ticket-sales*') ? 'active' : '' }}">
    <a href="{{ route('ticket-sales.index') }}" class="nav-link"><i class="fas fa-shopping-basket"></i><span>Penjualan Tiket</span></a>
</li>
@endcan

@can('view-sale-products')
<li class="menu-header">Data Menu Store</li>  {{-- Menu Produk --}}
@endcan

{{-- <li class="dropdown {{ request()->is('products*') ? 'active' : '' }}">
    <a href="#" class="menu-toggle nav-link has-dropdown">
        <i data-feather="grid"></i>
        <span>Produk</span>
    </a>
    <ul class="dropdown-menu"> 
        <li class="{{ request()->is('products*') ? 'active' : '' }}">
            <a class="nav-link" href="{{ route('products.index') }}">Produk Saya</a>
        </li>
        <li class="{{ request()->is('product-all') ? 'active' : '' }}">
            <a class="nav-link" href="#">Seluruh Produk</a>
        </li>
    </ul>
</li> --}}

@can('view-products')
<li class="dropdown {{ request()->is('products*') ? 'active' : '' }}">
    <a href="{{ route('products.index') }}" class="nav-link"><i class="fas fa-cube"></i><span>Produk</span></a>
</li>
@endcan

{{-- <li class="dropdown {{ request()->is('transaction') ? 'active' : '' }}">
    <a href="{{ route('transaction.index') }}" class="nav-link"><i data-feather="calendar"></i><span>Transaksi Baru</span></a>
</li> --}}

@can('view-sale-products')
<li class="dropdown {{ request()->is('transaction*') ? 'active' : '' }}">
    <a href="{{ route('transaction.index') }}" class="nav-link"><i class="fas fa-boxes"></i><span>Transaksi Baru</span></a>
</li>
@endcan

@can('view-sale-products')
<li class="dropdown {{ request()->is('product-sales*') ? 'active' : '' }}">
    <a href="{{ route('product-sales.index') }}" class="nav-link"><i class="fas fa-shopping-cart"></i><span>Penjualan Produk</span></a>
</li>
@endcan

@can('view-users')
<li class="menu-header">Menu Admin</li> {{-- Menu Tiket --}}
@endcan

@can('view-users')
<li class="dropdown {{ request()->is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}" class="nav-link"><i class="fas fa-id-badge"></i><span>Manajemen Akun</span></a>
</li>
<li class="dropdown {{ request()->is('report-tickets') ? 'active' : '' }}">
    <a href="{{ route('report.tickets') }}" class="nav-link"><i class="fas fa-file-alt"></i><span>Laporan Sistem Tiket</span></a>
</li>
<li class="dropdown {{ request()->is('report-store') ? 'active' : '' }}">
    <a href="{{ route('report.store') }}" class="nav-link"><i class="fas fa-file-contract"></i><span>Laporan POS Store</span></a>
</li>
@endcan

{{-- <li class="dropdown {{ request()->is('category*') ? 'active' : '' }}">
    <a href="#" class="nav-link"><i data-feather="calendar"></i><span>Laporan</span></a>
</li> --}}

@can('view-users')
<li class="dropdown {{ request()->is('log-activities') ? 'active' : '' }}">
    <a href="{{ route('log-activities.index') }}" class="nav-link"><i class="fas fa-clipboard-list"></i><span>Aktivitas Petugas</span></a>
</li>
@endcan