@extends('layout.main')
@section('title','Dashboard')

@section('content')
<section class="section">
    <div class="row ">
      <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
          <div class="card-statistic-4">
            <div class="align-items-center justify-content-between">
              <div class="row ">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                  <div class="card-content">
                    <h5 class="font-15">Jumlah Tiket Tersedia</h5>
                    <h2 class="mb-3 font-18">{{ number_format($dashboard['today']['ticket_available'], 0, '', '.') }}</h2>
                    @if ($dashboard['state']['ticket_available'])
                      <p class="mb-0"><i data-feather="chevrons-up" class="col-green"></i> Increase</p>
                    @else
                      <p class="mb-0"><i data-feather="chevrons-down" class="col-orange"></i> Decrease</p>
                    @endif
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                  <div class="banner-img">
                    <img src="assets/img/banner/1.png" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
          <div class="card-statistic-4">
            <div class="align-items-center justify-content-between">
              <div class="row ">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                  <div class="card-content">
                    <h5 class="font-15"> Tiket Terjual Hari ini</h5>
                    <h2 class="mb-3 font-18">{{ number_format($dashboard['today']['ticket_sold'], 0, '', '.') }}</h2>
                    @if ($dashboard['state']['ticket_sold'])
                      <p class="mb-0"><i data-feather="chevrons-up" class="col-green"></i> Increase</p>
                    @else
                      <p class="mb-0"><i data-feather="chevrons-down" class="col-orange"></i> Decrease</p>
                    @endif
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                  <div class="banner-img">
                    <img src="assets/img/banner/2.png" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
          <div class="card-statistic-4">
            <div class="align-items-center justify-content-between">
              <div class="row ">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                  <div class="card-content">
                    <h5 class="font-15">Tiket Dipindai Hari ini</h5>
                    <h2 class="mb-3 font-18">{{ number_format($dashboard['today']['ticket_scanned'], 0, '', '.') }}</h2>
                    @if ($dashboard['state']['ticket_scanned'])
                      <p class="mb-0"><i data-feather="chevrons-up" class="col-green"></i> Increase</p>
                    @else
                      <p class="mb-0"><i data-feather="chevrons-down" class="col-orange"></i> Decrease</p>
                    @endif
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                  <div class="banner-img">
                    <img src="assets/img/banner/3.png" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
          <div class="card-statistic-4">
            <div class="align-items-center justify-content-between">
              <div class="row ">
                <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                  <div class="card-content">
                    <h5 class="font-15">Uang Masuk Hari ini</h5>
                    <h2 class="mb-2 font-18">Rp. {{ number_format($dashboard['today']['income'], 0, '', '.') }}</h2>
                    {{-- @if ($dashboard['state']['income'])
                      <p class="mb-0"><i data-feather="chevrons-up" class="col-green"></i> Increase</p>
                    @else
                      <p class="mb-0"><i data-feather="chevrons-down" class="col-orange"></i> Decrease</p>
                    @endif --}}
                    <ul class="mb-0 font-12 pl-3 font-bold" style="line-height: 22px">
                      @foreach ($dashboard['today']['income_type'] as $payment)
                        <li>{{ $payment->name }} = Rp. {{ number_format($payment->ticket_sales_sum_final_amount ?? 0, 0, '', '.') }}</li>
                      @endforeach
                    </ul>
                  </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 pl-0">
                  <div class="banner-img">
                    <img src="assets/img/banner/4.png" alt="">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-12 col-lg-12">
        <div class="card ">
          <div class="card-header">
            <h4>Penjualan Mingguan</h4>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-9">
                <div id="chart-weekly"></div>
                {{-- <div class="row mb-0">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="list-inline text-center">
                      <div class="list-inline-item p-r-30"><i data-feather="arrow-up-circle"
                          class="col-green"></i>
                        <h5 class="m-b-0">$675</h5>
                        <p class="text-muted font-14 m-b-0">Weekly Earnings</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="list-inline text-center">
                      <div class="list-inline-item p-r-30"><i data-feather="arrow-down-circle"
                          class="col-orange"></i>
                        <h5 class="m-b-0">$1,587</h5>
                        <p class="text-muted font-14 m-b-0">Monthly Earnings</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="list-inline text-center">
                      <div class="list-inline-item p-r-30"><i data-feather="arrow-up-circle"
                          class="col-green"></i>
                        <h5 class="mb-0 m-b-0">$45,965</h5>
                        <p class="text-muted font-14 m-b-0">Yearly Earnings</p>
                      </div>
                    </div>
                  </div>
                </div> --}}
              </div>
              <div class="col-lg-3">
                <div class="row mt-4">
                  <div class="col-7 col-xl-7 mb-3"><strong>Hari Ini</strong></div>
                  <div class="col-5 col-xl-5 mb-3">
                    <span class="text-big"><strong>: {{ Carbon\Carbon::now()->translatedFormat('l'); }} </strong></span>
                  </div>
                  <div class="col-7 col-xl-7 mb-3">Total Pengunjung</div>
                  <div class="col-5 col-xl-5 mb-3">
                    <span class="text-big">: {{ $dashboard['today']['ticket_sold'] }}</span>
                    @if (!empty($dashboard['today']['ticket_sold_percent']) && $dashboard['today']['ticket_sold_percent'] <= 0)
                      <sup class="text-danger">{{ $dashboard['today']['ticket_sold_percent'] ?? 0 }}%</sup>
                    @else
                      <sup class="col-green">{{ $dashboard['today']['ticket_sold_percent'] ?? 0 }}%</sup>
                    @endif
                  </div>
                  <div class="col-7 col-xl-7 mb-3">Pengunjung Gratis</div>
                  <div class="col-5 col-xl-5 mb-3">
                    <span class="text-big">: {{ $dashboard['weekly']['today']['free_ticket'] ?? 0 }}</span></span>
                    @if (!empty($dashboard['weekly']['percent']['free_ticket']) && $dashboard['weekly']['percent']['free_ticket'] <= 0)
                      <sup class="text-danger">{{ $dashboard['weekly']['percent']['free_ticket'] ?? 0 }}%</sup>
                    @else
                      <sup class="col-green">{{ $dashboard['weekly']['percent']['free_ticket'] ?? 0 }}%</sup>
                    @endif
                  </div>
                  <div class="col-7 col-xl-7 mb-3">Pengunjung Domestik</div>
                  <div class="col-5 col-xl-5 mb-3">
                    <span class="text-big">: {{ $dashboard['weekly']['today']['domestic_ticket'] ?? 0 }}</span></span>
                    @if (!empty($dashboard['weekly']['percent']['domestic_ticket']) && $dashboard['weekly']['percent']['domestic_ticket'] <= 0)
                      <sup class="text-danger">{{ $dashboard['weekly']['percent']['domestic_ticket'] ?? 0 }}%</sup>
                    @else
                      <sup class="col-green">{{ $dashboard['weekly']['percent']['domestic_ticket'] ?? 0 }}%</sup>
                    @endif
                  </div>
                  <div class="col-7 col-xl-7 mb-3">Pengunjung Nasional</div>
                  <div class="col-5 col-xl-5 mb-3">
                    <span class="text-big">: {{ $dashboard['weekly']['today']['national_ticket'] ?? 0 }}</span></span>
                    @if (!empty($dashboard['weekly']['percent']['national_ticket']) && $dashboard['weekly']['percent']['national_ticket'] <= 0)
                      <sup class="text-danger">{{ $dashboard['weekly']['percent']['national_ticket'] ?? 0 }}%</sup>
                    @else
                      <sup class="col-green">{{ $dashboard['weekly']['percent']['national_ticket'] ?? 0 }}%</sup>
                    @endif
                  </div>
                  <div class="col-7 col-xl-7 mb-3" style="white-space: nowrap">Pengunjung Internasional</div>
                  <div class="col-5 col-xl-5 mb-3">
                    <span class="text-big">: {{ $dashboard['weekly']['today']['international_ticket'] ?? 0 }}</span></span>
                    @if (!empty($dashboard['weekly']['percent']['international_ticket']) && $dashboard['weekly']['percent']['international_ticket'] <= 0)
                      <sup class="text-danger">{{ $dashboard['weekly']['percent']['international_ticket'] ?? 0 }}%</sup>
                    @else
                      <sup class="col-green">{{ $dashboard['weekly']['percent']['international_ticket'] ?? 0 }}%</sup>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-xl-3">
        <div class="card">
          <div class="card-header">
            <h4>Data Jenis Tiket Tersedia</h4>
          </div>
          <div class="card-body px-5 text-center py-0">
            <div id="chart-type-tickets"></div>
          </div>
        </div>
      </div>
      <div class="col-12 col-xl-9">
        <div class="card">
          <div class="card-header">
            <h4>Pengunjung Bulanan Tahun {{ Carbon\Carbon::now()->format('Y') }}</h4>
          </div>
          <div class="card-body px-5 text-center py-0">
            <div id="chart-monthly"></div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@push('addons-script')
    <!-- JS Libraies -->
  <script src="{{ asset('assets/bundles/apexcharts/apexcharts.min.js') }}"></script>
<script>
  $(document).ready(function () {
    chartWeekly()
    chartTypeTickets()
    chartMonthly()
  });

  function chartWeekly() {
    let options = {
        chart: {
            height: 280,
            type: "line",
            shadow: {
                enabled: true,
                color: "#000",
                top: 18,
                left: 7,
                blur: 10,
                opacity: 1
            },
            toolbar: {
                show: false
            }
        },
        colors: ["#786BED", "#999b9c"],
        dataLabels: {
            enabled: true
        },
        stroke: {
            curve: "smooth"
        },
        series: [{
            name: "Pengunjung",
            data: @json($dashboard['weekly']['graph']['qty']),
          }],
        grid: {
            borderColor: "#e7e7e7",
            row: {
                colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
                opacity: 0.0
            }
        },
        markers: {
            size: 6
        },
        xaxis: {
            categories: @json($dashboard['weekly']['graph']['label']),

            labels: {
                style: {
                    colors: "#9aa0ac"
                }
            }
        },
        yaxis: {
            title: {
                text: "Pengunjung"
            },
            labels: {
                style: {
                    color: "#9aa0ac"
                }
            }
        },
        legend: {
            position: "top",
            horizontalAlign: "right",
            floating: true,
            offsetY: -25,
            offsetX: -5
        }
    };
    let chart = new ApexCharts(document.querySelector("#chart-weekly"), options);
    chart.render();
  }

  function chartTypeTickets() {
    var options = {
      // colors: ['#2E93fA', '#66DA26', '#546E7A', '#E91E63'];
      series: @json($dashboard['type_ticket']['inventory']),
      chart: {
        type: 'donut',
        height: '350px',
      },
      legend: {
        position: 'bottom'
      },
      colors:['#ff742e', '#bc0000', '#31ba31','#1c1eb1'],
      breakpoint: 180,
      labels: @json($dashboard['type_ticket']['label']),
      plotOptions: {
        pie: {
          donut: {
            labels: {
              show: true,
            }
          }
        }
      },
      responsive: [{
        breakpoint: 180,
        options: {
          chart: {
            width: '90%',
          },
          legend: {
            position: 'bottom'
          },
        }
      }]
    };

      var chart = new ApexCharts(document.querySelector("#chart-type-tickets"), options);
      chart.render();
  }

  function chartMonthly() {
    var options = {
        chart: {
            height: 340,
            type: 'bar',
            stacked: true,
            toolbar: {
                show: false
            },
            zoom: {
                enabled: true
            }
        },
        responsive: [{
            breakpoint: 480,
            options: {
                legend: {
                    position: 'bottom',
                    offsetX: -10,
                    offsetY: 0
                }
            }
        }],
        plotOptions: {
            bar: {
                horizontal: false,
            },
        },
        series: [{
            name: "{{ $dashboard['ticket_sales']['visitor']['free_ticket']['name'] }}",
            data: @json($dashboard['ticket_sales']['visitor']['free_ticket']['data'])
        }, {
            name: "{{ $dashboard['ticket_sales']['visitor']['domestic_ticket']['name'] }}",
            data: @json($dashboard['ticket_sales']['visitor']['domestic_ticket']['data'])
        }, {
            name: "{{ $dashboard['ticket_sales']['visitor']['national_ticket']['name'] }}",
            data: @json($dashboard['ticket_sales']['visitor']['national_ticket']['data'])
        }, {
            name: "{{ $dashboard['ticket_sales']['visitor']['international_ticket']['name'] }}",
            data: @json($dashboard['ticket_sales']['visitor']['international_ticket']['data'])
        }],
        xaxis: {
            categories: @json($dashboard['ticket_sales']['label']),
            labels: {
                style: {
                    colors: "#9aa0ac"
                }
            }
        },
        yaxis: {
            labels: {
                style: {
                    color: "#9aa0ac"
                }
            }
        },
        legend: {
            position: 'top',
            offsetY: 40,
            show: false,
        },
        fill: {
            opacity: 1
        },
    }

    var chart = new ApexCharts(
        document.querySelector("#chart-monthly"),
        options
    );

    chart.render();
}
</script>
@endpush