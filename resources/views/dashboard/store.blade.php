@extends('layout.main')

@section('title', 'Dashboard')

@section('content')
<section class="section">
    <div class="row ">
        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-statistic-4">
                    <div class="align-items-center justify-content-between">
                        <div class="row ">
                            <div class="col-lg-6 col-md-6 co    l-sm-6 col-xs-6 pr-0 pt-3">
                                <div class="card-content">
                                    <h5 class="font-15">Transaksi Hari Ini</h5>
                                    <h2 class="mb-3 font-18">{{ $dashboard['today']['sales'] }}</h2>
                                    {{-- <p class="mb-0">
                                        <span class="col-green">10%</span> 
                                        Meningkat
                                    </p> --}}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                                <div class="banner-img">
                                    <img src="assets/img/banner/1.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-statistic-4">
                    <div class="align-items-center justify-content-between">
                        <div class="row ">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                                <div class="card-content">
                                    <h5 class="font-15"> Item Terjual Hari ini</h5>
                                    <h2 class="mb-3 font-18">{{ $dashboard['today']['product_sales'] }} </h2>
                                    {{-- <p class="mb-0">
                                        <span class="col-orange">09%</span> 
                                        Menurun
                                    </p> --}}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                                <div class="banner-img">
                                    <img src="assets/img/banner/2.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-statistic-4">
                    <div class="align-items-center justify-content-between">
                        <div class="row ">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                                <div class="card-content">
                                    <h5 class="font-15">Banyak Jenis Produk</h5>
                                    <h2 class="mb-3 font-18">{{ $dashboard['today']['products'] }}</h2>
                                    {{-- <p class="mb-0">
                                        <span class="col-green">18%</span>
                                        Meningkat
                                    </p> --}}
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                                <div class="banner-img">
                                    <img src="assets/img/banner/3.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="card-statistic-4">
                    <div class="align-items-center justify-content-between">
                        <div class="row ">
                            <div class="col-lg-7 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                                <div class="card-content">
                                    <h5 class="font-15">Uang Masuk Hari Ini</h5>
                                    <h2 class="mb-2 font-18">Rp. {{ number_format($dashboard['today']['income'],0,',','.') }}</h2>
                                    <ul class="mb-0 font-12 pl-3 font-bold" style="line-height: 22px">
                                        @foreach ($dashboard['today']['income_type'] as $payment)
                                            <li>{{ $payment->name }} = Rp. {{ number_format($payment->product_sales_sum_final_amount ?? 0, 0, '', '.') }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6 pl-0">
                                <div class="banner-img">
                                    <img src="assets/img/banner/4.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-sm-12 col-lg-6">
            <div class="card ">
                <div class="card-header">
                    <h4>Penjualan Mingguan</h4>
                </div>
                <div class="card-body">
                    <div id="chart-weekly"></div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-lg-6">
            <div class="card ">
                <div class="card-header">
                    <h4>Penjualan Bulanan</h4>
                </div>
                <div class="card-body">
                    <canvas id="chart-monthly" height="105"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h4>10 Produk Terlaris</h4>
                </div>
                <div class="card-body p-0">
                    <table class="table table-striped mb-0">
                        <tr>
                            <th class="text-center">No.</th>
                            <th></th>
                            <th>Nama</th>
                            <th>Kode</th>
                            <th class="text-right">Harga</th>
                            {{-- <th>Status</th> --}}
                            <th class="text-right pr-4">Total</th>
                            <th></th>
                        </tr>
                        @forelse ($dashboard['bestsellers'] as $seller)
                            <tr>
                                <td class="p-0 text-center">{{ $loop->iteration }}</td>
                                <td>
                                    <img class="image-product" alt="image" src="{{ $seller->product != null ? $seller->product->image : asset('assets/img/not_found.png') }}">
                                </td>
                                <td>{{ $seller->name }}</td>
                                <td>{{ $seller->code }}</td>
                                <td class="text-right">Rp. {{ number_format($seller->price, 0, '', '.') }}</td>
                                <td class="text-right pr-4">{{ $seller->amount }}</td>
                                <td><a href="{{ route('products.show', $seller->product_id) }}" class="btn btn-outline-warning">Detail</a></td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-center text-muted" colspan="8"><h6>Belum ada produk terjual.</h6></td>
                            </tr>
                        @endforelse
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h4>Riwayat Produk Terjual Terakhir</h4>
                </div>
                <div class="card-body p-0">
                    <table class="table mb-0">
                        <thead>
                            <tr>
                                <th class="text-center">No.</th>
                                <th>Nama</th>
                                <th class="text-right">QTY</th>
                                <th class="text-right">Harga</th>
                                <th class="text-center">Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($dashboard['latest'] as $latest)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>{{ $latest->name }} </td>
                                    <td class="text-right">{{ $latest->qty }}</td>
                                    <td class="text-right text-nowrap">Rp. {{ number_format($latest->price, 0, '', '.') }}</td>
                                    <td class="text-center">{{ date('d-M-Y', strtotime($latest->created_at)) }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center text-muted" colspan="5"><h6>Belum ada produk terjual.</h6></td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('addons-style')
<style>
    .image-product {
        width: auto;
        height: auto;
        max-width: 75px;
        max-height: 65px;
    }
</style>
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/chartjs/chart.min.js') }}"></script>
<script src="{{ asset('assets/bundles/apexcharts/apexcharts.min.js') }}"></script>
<script>
    $(document).ready(function () {
        chartWeekly()
        chartMonthly()
    });
    function chartWeekly() {
        var options = {
            chart: {
                height: 250,
                type: "line",
                shadow: {
                    enabled: true,
                    color: "#000",
                    top: 18,
                    left: 7,
                    blur: 10,
                    opacity: 1
                },
                toolbar: {
                    show: false
                }
            },
            colors: ["#786BED", "#999b9c"],
            dataLabels: {
                enabled: true,
                // offsetX: 3
            },
            stroke: {
                curve: "smooth",
                show: true,
            },
            series: [{
                name: "Penjualan",
                data: @json($dashboard['weekly']['qty'])
              }],
            grid: {
                borderColor: "#e7e7e7",
                row: {
                    colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
                    opacity: 0.2
                },
                padding: {
                    left: 10,
                    right: 30 
                },
            },
            markers: {
                size: 6,
                // offsetX: 15,
            },
            xaxis: {
                categories: @json($dashboard['weekly']['label']),
                labels: {
                    style: {
                        colors: "#9aa0ac"
                    },
                    // offsetX: 5,
                },
            },
            yaxis: {
                title: {
                    text: "Transaksi"
                },
                labels: {
                    style: {
                        color: "#9aa0ac"
                    }
                }
            },
            legend: {
                position: "top",
                horizontalAlign: "right",
                floating: true,
                offsetY: -25,
                offsetX: -5
            }
        };
        var chart = new ApexCharts(document.querySelector("#chart-weekly"), options);
        chart.render();
    }

    function chartMonthly() {
        var ctx = document.getElementById("chart-monthly").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: @json($dashboard['monthly']['month']),
                datasets: [{
                    label: 'Transaksi',
                    data: @json($dashboard['monthly']['qty']),
                    borderWidth: 2,
                    backgroundColor: 'rgba(255,164,38,.9)',
                    borderColor: 'rgba(255,164,38,.9)',
                    borderWidth: 2.5,
                    pointBackgroundColor: '#ffffff',
                    pointRadius: 4
                }]
            },
            options: {
                layout: {
                    padding: 5
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            drawBorder: false,
                            color: '#f2f2f2',
                        },
                        ticks: {
                            beginAtZero: true,
                            // stepSize: 1,
                            fontColor: "#9aa0ac", // Font Color
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            fontColor: "#9aa0ac", // Font Color
                        }
                    }]
                },
            }
        });
    }

</script>
@endpush