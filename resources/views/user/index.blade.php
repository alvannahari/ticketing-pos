@extends('layout.main')

@section('title', 'Manajemen Akun')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card card-warning">
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-users" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Nama</th>
                                    <th>Username </th>
                                    <th>Role</th>
                                    <th class="text-center">Created At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Add User-->
<div class="modal fade" id="modal-add-user" tabindex="-1" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Tambah Pengguna Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form class="mt-3" method="POST" action="#" id="form-add-user">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" placeholder="nama petugas" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" placeholder="username petugas" name="username" required>
                    </div>
                    <div class="form-group">
                        <label>Role</label>
                        <select name="role_id" class="form-control">
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" placeholder="******" name="password">
                    </div>
                    <div class="form-group">
                        <label>Konfirmasi Password</label>
                        <input type="password" class="form-control" placeholder="******" name="password_confirmation">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-warning" id="btn-add-user">Tambahkan Petugas </button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal change Role -->
<div class="modal fade" id="modal-change-role" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ganti/Update Role Petugas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form-change-role">
                    @csrf
                    <select name="role_id" class="form-control">
                        @foreach ($roles as $role)
                            <option value="{{ $role->id }}">Petugas {{ $role->name }}</option>
                        @endforeach
                    </select>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-info" id="btn-change-role">Update Role</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal change Password -->
<div class="modal fade" id="modal-change-password" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ganti/Update Password Petugas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form action="#" id="form-change-password">
                    @csrf
                    <div class="fom-group py-2">
                        <label for="input-change-password">Password Baru</label>
                        <input type="password" class="form-control" id="input-change-password" placeholder="******" name="new_password">
                    </div>
                    <div class="fom-group py-2 mt-2">
                        <label for="input-change-password-confirm">Konfirmasi Password Baru</label>
                        <input type="password" class="form-control" id="input-change-password-confirm" placeholder="******" name="new_password_confirmation">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" id="btn-change-password">Update Password</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus petugas ini ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="btn-delete-user">Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="assets/bundles/datatables/datatables.min.css">
<link rel="stylesheet" href="assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="assets/bundles/datatables/datatables.min.js"></script>
<script src="assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<!-- Page Specific JS File -->
<script src="assets/js/page/datatables.js"></script>
<script>
    let user_id = 0;
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        })
        let dt_users = $('#table-users').DataTable( {
            "processing": true,
            "ajax": "{!! url()->current() !!}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "name"},
                { "data": "username"},
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        return 'Petugas '+row.roles[0].name;
                    }
                },
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        return `
                            <button href="#" class="btn btn-info" onclick="changeRole(`+row.id+`,`+row.role_id+`)"><i class="fas fa-pencil-alt"></i> Ganti Role</button>
                            <button class="btn btn-success" onclick="changePassword(`+row.id+`)"><i class="fa fa-key"></i> Ganti Password</button>
                            <button class="btn btn-danger" onclick="deleteUser(`+row.id+`)"><i class="fa fa-trash"></i> Hapus</button>
                            `;
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                $("#table-users_length").parent().before(`
                    <div class="col-12 col-md-4 text-center text-md-left">
                        <button class="btn btn-rounded btn-warning mb-2" type="button" data-toggle="modal" data-target="#modal-add-user"><i class="fa fa-plus"></i> Tambah Petugas</button>
                    </div>
                `);
                $("#table-users_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
                $("#table-users_length").css('text-align', 'center');
                $("#table-users_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
            }   
        });
        dt_users.on( 'order.dt search.dt', function () {
            dt_users.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#btn-add-user').click(function (e) { 
            e.preventDefault();
            let btn = $(this);
    
            $.ajax({
                type: "POST",
                url: "{{ route('users.store') }}",
                data: $('#form-add-user').serialize(),
                dataType: "json",
                beforeSend() {
                    btn.addClass("btn-progress")
                },
                success: function (response) {
                    if (response.status) {
                        dt_users.ajax.reload(function () {
                            $('#modal-add-user').modal('hide');
                            showAlert('success',response.message,'Akun Petugas')
                        }, false);
                    } else {
                        for (let key of Object.keys(response.errors)) {
                            $('#form-add-user [name="'+key+'"]').addClass('is-invalid');
                            $('#form-add-user [name="'+key+'"]').after('<div class="invalid-feedback">'+response.errors[key]+'.</div>');
                        }
                        showAlert('error', 'Silakan periksa formulir', 'Akun Petugas')
                    }
                    btn.removeClass('btn-progress');
                }, 
                error: function(xhr, status, error) {
                    btn.removeClass('btn-progress');
                    showAlert('error', 'Terjadi Kesalahan', 'Akun Petugas')
                    console.log(xhr.responseText);
                }
            });
        });

        $('#btn-change-role').click(function (e) { 
            e.preventDefault();
            let btn = $(this);
            let url_change_role = "{{ route('user.change-role', ':id') }}";
    
            $.ajax({
                type: "POST",
                url: url_change_role.replace(':id', user_id),
                data: $('#form-change-role').serialize(),
                dataType: "json",
                beforeSend() {
                    btn.addClass("btn-progress")
                },
                success: function (response) {
                    dt_users.ajax.reload(function () {
                        $('#modal-change-role').modal('hide');
                        showAlert('success',response.message,'Akun Petugas')
                    }, false);
                    $('#modal-change-role').modal('hide');
                    btn.removeClass('btn-progress');
                }, 
                error: function(xhr, status, error) {
                    btn.removeClass('btn-progress');
                    showAlert('error', 'Terjadi Kesalahan', 'Akun Petugas')
                    console.log(xhr.responseText);
                }
            });
        });
        
        $('#btn-change-password').click(function (e) { 
            e.preventDefault();
            let btn = $(this);
            let url_change_password = "{{ route('user.change-password', ':id') }}";
    
            $.ajax({
                type: "POST",
                url: url_change_password.replace(':id', user_id),
                data: $('#form-change-password').serialize(),
                dataType: "json",
                beforeSend() {
                    btn.addClass("btn-progress")
                },
                success: function (response) {
                    if (response.status) {
                        dt_users.ajax.reload(function () {
                            $('#modal-change-password').modal('hide');
                            showAlert('success',response.message,'Akun Petugas')
                        }, false);
                        $('#modal-change-role').modal('hide');
                    } else {
                        for (let key of Object.keys(response.errors)) {
                            $('#form-change-password [name="'+key+'"]').addClass('is-invalid');
                            $('#form-change-password [name="'+key+'"]').after('<div class="invalid-feedback">'+response.errors[key]+'.</div>');
                        }
                        showAlert('error', 'Silakan periksa formulir', 'Akun Petugas')
                    }
                    btn.removeClass('btn-progress');
                }, 
                error: function(xhr, status, error) {
                    btn.removeClass('btn-progress');
                    showAlert('error', 'Terjadi Kesalahan', 'Akun Petugas')
                    console.log(xhr.responseText);
                }
            });
        });

        $('#btn-delete-user').click(function (e) { 
            e.preventDefault();
            let btn = $(this);
            let url_delete = "{{ route('users.destroy', ':id') }}";
    
            $.ajax({
                type: "DELETE",
                url: url_delete.replace(':id', user_id),
                success: function (response) {
                    dt_users.ajax.reload(function () {
                        $('#modal-delete').modal('hide');
                        showAlert('success',response.message,'Berhasil')
                    }, false);
                },
            });
        });
    });

    function deleteUser(id) {
        user_id = id;
        $('#modal-delete').modal('show');
    }

    function changeRole(id, role_id) {
        user_id = id;
        $("#modal-change-role select").val(role_id).trigger("change");
        $('#modal-change-role').modal('show');
    }

    function changePassword(id) {
        user_id = id;
        $('#modal-change-password input[type="password"]').val('');
        $('#modal-change-password').modal('show');
    }
    
</script>
@endpush

