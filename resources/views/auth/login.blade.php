<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>{{ config('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Login Admin & Staff " name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel='shortcut icon' type='image/x-icon' href="{{ asset('assets/img/favicon.ico') }}" />

    <!-- App css -->
    <link href="{{ asset('assets/login/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/login/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Main -->
    <link href="{{ asset('assets/login/login.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('assets/login/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/login/bootstrap.min.js') }}"></script>
</head>

<body class="loading authentication-bg authentication-bg-pattern">
    <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto" style="max-width: 1400px;">
        <div class="card card0 border-0">
            <div class="row d-flex">
                <div class="col-lg-6 order-1 order-lg-2 px-lg-0">
                    <div class="card1">
                        {{-- <div class="row">
                            <img src="https://i.imgur.com/CXQmsmF.png" class="logo">
                        </div> --}}
                        <div class="justify-content-center border-line">
                            <div id="image-thumbnail" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#image-thumbnail" data-slide-to="0" class="active"></li>
                                    <li data-target="#image-thumbnail" data-slide-to="1" class=""></li>
                                    <li data-target="#image-thumbnail" data-slide-to="2" class=""></li>
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img class="d-block img-fluid image" src="{{ asset('assets/login/banner_1.jpg') }}" alt="Admin">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block img-fluid image" src="{{ asset('assets/login/banner_2.jpg') }}" alt="Admin">
                                    </div>
                                    <div class="carousel-item">
                                        <img class="d-block img-fluid image" src="{{ asset('assets/login/banner_3.jpg') }}" alt="Admin">
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#image-thumbnail" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden"></span>
                                </a>
                                <a class="carousel-control-next" href="#image-thumbnail" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 order-2 order-lg-1">
                    <div class="card2 card border-0 px-0 py-5">
                        <div class="row mb-2">
                            {{-- <h6 class="mb-0 mr-4 mt-2">Sign in with</h6>
                            <div class="facebook text-center mr-3"><div class="fa fa-facebook"></div></div>
                            <div class="twitter text-center mr-3"><div class="fa fa-twitter"></div></div>
                            <div class="linkedin text-center mr-3"><div class="fa fa-linkedin"></div></div> --}}
                            <div class="col-12 text-center px-0">
                                <img class="logo" src="{{ asset('assets/login/logo-1.png') }}" alt="" srcset="">
                                <img class="logo" src="{{ asset('assets/login/logo-2.png') }}" alt="" srcset="">
                                <img class="logo" src="{{ asset('assets/login/logo-3.png') }}" alt="" srcset="">
                            </div>
                        </div>
                        <div class="row py-3 px-5">
                            <div class="col-12 text-center px-2 px-lg-5">
                                <h3 class="mb-1" style="letter-spacing: -0.8px;">Selamat Datang</h3>
                                <p class="mb-0" style="line-height: 20px;">Masukan pasangan ID dan password untuk mengakses Sistem Informasi Museum dan Galeri SBY*ANI.</p>
                            </div>
                        </div>
                        <div class="row py-4 px-5">
                            <div class="col-12 px-2 px-lg-5">
                                <form action="{{ route('login.store') }}" method="POST">
                                    @csrf
                                    <div class="row px-3">
                                        <label class="mb-1"><h6 class="mb-0 text-sm">ID Pegawai</h6></label>
                                        <input class="mb-4" type="text" name="username" required="" placeholder="Masukkan ID Pegawai">
                                    </div>
                                    <div class="row px-3 mb-3">
                                        <label class="mb-1"><h6 class="mb-0 text-sm">Password</h6></label>
                                        <input type="password" name="password" required="" placeholder="Masukkan Password">
                                    </div>
                                    <div class="row px-3 mb-3">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input id="chk1" type="checkbox" name="chk" class="custom-control-input"> 
                                            <label for="chk1" class="custom-control-label text-sm">Remember me</label>
                                        </div>
                                    </div>
                                    @if(session()->has('message'))
                                        <span class="help-block required text-danger d-block text-sm" style="font-weight: 500;">{{ session()->get('message') }}</span>
                                    @endif
                                    <div class="row p-3">
                                        <button type="submit" class="btn text-center w-100 py-2 text-white font-bold" style="    background-color: #FF8036;">Masuk</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        {{-- <div class="row mb-4 px-3">
                            <small class="font-weight-bold">Lupa akun ini ? <span class="text-primary">Segera hubungi admin atau owner.</span></small>
                        </div> --}}
                    </div>
                    <div class="bg-dark py-4" style="position: absolute;right: 0;">
                        <div class="row">
                            <small class="ml-4 ml-sm-5 mb-2">Copyright <a href="https://krakatio.com" target=”_blank” >&copy;Krakatio 2023</a>. All rights reserved.</small>
                            <div class="social-contact ml-4 ml-sm-auto mr-3">
                                {{-- <span>Kontak Admin : +08563194806</span> --}}
                                <span class="fa fa-facebook mr-4 text-sm"></span>
                                <span class="fa fa-google-plus mr-4 text-sm"></span>
                                <span class="fa fa-linkedin mr-4 text-sm"></span>
                                <span class="fa fa-twitter mr-4 mr-sm-5 text-sm"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('.carousel').carousel({
            interval: 3000
        })
    </script>

</body>
</html>