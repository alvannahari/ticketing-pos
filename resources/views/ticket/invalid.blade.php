@extends('layout.main')
@section('title','Daftar Tiket Yang Invalid')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card card-warning">
                {{-- <div class="card-header">
                    <h4>Basic DataTables</h4>
                </div> --}}
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-tickets" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Jenis Tiket</th>
                                    <th>Barcode</th>
                                    <th>Harga @</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Dibuat</th>
                                    <th class="text-center">Diperbarui</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus Tiket ini ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="btn-delete-ticket">Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>

<script>
    var ticket_id = 1;

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        })
        let dt_tickets = $('#table-tickets').DataTable( {
            processing: true,
            serverSide: true,
            paging:true,
            ajax: "{!! url()->current() !!}",
            pageLength: 50,
            order: [[5, 'asc']],
            scrollY: 600,
            scrollCollapse: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', sClass: "text-center", orderable: false, searchable: false, bSortable: false},
                {data: 'type_name', name: 'type.name'},
                {data: 'barcode', name: 'barcode'},
                {data: 'price', name: 'type.price', sClass: "text-right", type: 'natural'},
                {data: 'status', name: 'is_valid',sClass: "text-center"},
                {data: 'created_at', name: 'created_at', sClass: "text-center"},
                {data: 'updated_at', name: 'updated_at', sClass: "text-center"},
                {data: 'action', name: 'action', sClass: "text-center", orderable: false, searchable: false, bSortable: false},
            ],
        });
        dt_tickets.on( 'order.dt search.dt', function () {
            dt_tickets.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#btn-delete-ticket').click(function (e) { 
            e.preventDefault();
            let url_delete = "{{ route('tickets.destroy', ':id') }}";

            $.ajax({
                type: "DELETE",
                url: url_delete.replace(':id', ticket_id),
                success: function (response) {
                    if (response.status) {
                        dt_tickets.ajax.reload(function () {
                            $('#modal-delete').modal('hide');
                            showAlert('success',response.message,'Berhasil')
                        }, false);
                    }
                },
            });
        });
    });

    function deleteTicket(id) {
        ticket_id = id;
        $('#modal-delete').modal('show');
    }
</script>
@endpush