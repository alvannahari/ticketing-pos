@extends('layout.main')

@section('title', 'Penjualan Baru')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body pb-1" style="height: calc(100vh - 200px);">
                    <form id="form-sale" method="POST">
                        <div class="row">
                            <div class="col-12 col-lg-9 col-xl-8">
                                <div class="table-responsive" style="height: calc(100vh - 240px);">
                                    <table class="table" id="table-sale">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Nama Item</th>
                                                <th scope="col">Kode</th>
                                                <th scope="col">Harga</th>
                                                <th scope="col" class="text-center">Banyak Item</th>
                                                <th scope="col" class="text-right">Total</th>
                                                <th scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td scope="row"></td>
                                                <td colspan="6"><button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-add-item">Tambah Item</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-12 col-lg-3 col-xl-4 px-1">
                                @csrf
                                <div class="py-4 pl-2 mb-2" style="color: white; background-color:#353c48 ">
                                    <h3>Total <span class="mr-3">:</span> Rp. 0,00</h3>
                                </div>
                                <div class="form-group">
                                    <label>Nama Pembeli</label>
                                    <input type="text" name="cust_name" class="form-control" form="form-sale" placeholder="-">
                                </div>
                                <div class="row">
                                    <div class="col-12 col-xl-6">
                                        <div class="form-group">
                                            <label>Metode Pembayaran</label>
                                            <select name="type_payment_id" class="form-control" form="form-sale">
                                                @foreach ($payments as $payment)
                                                    <option value="{{ $payment->id }}">{{ $payment->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-6">
                                        <div class="form-group">
                                            <label>Sub Total</label>
                                            <div class="input-group">
                                                <span class="input-group-text" id="total_price">Rp.</span>
                                                <input type="hidden" name="total_price" form="form-sale">
                                                <input type="number" name="total_price" class="form-control" disabled placeholder="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-xl-6">
                                        <div class="form-group">
                                            <label>Diskon</label>
                                            <div class="input-group">
                                                <span class="input-group-text" id="discount">Rp.</span>
                                                <input type="number" name="discount" class="form-control" form="form-sale" min="0" placeholder="0" aria-describedby="discount">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-6">
                                        <div class="form-group">
                                            <label>Total</label>
                                            <div class="input-group">
                                                <span class="input-group-text" id="final_amount">Rp.</span>
                                                <input type="hidden" name="final_amount" form="form-sale">
                                                <input type="number" name="final_amount" class="form-control" disabled placeholder="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-xl-6">
                                        <div class="form-group">
                                            <label>Uang Pembayaran</label>
                                            <div class="input-group">
                                                <span class="input-group-text" id="paid_amount">Rp.</span>
                                                <input type="number" name="paid_amount" class="form-control" form="form-sale" aria-describedby="paid_amount" placeholder="0">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-xl-6">
                                        <div class="form-group">
                                            <label>Uang Kembalian</label>
                                            <div class="input-group">
                                                <span class="input-group-text" id="change">Rp.</span>
                                                <input type="hidden" name="change" form="form-sale">
                                                <input type="number" name="change" class="form-control" disabled placeholder="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Note.</label>
                                    <textarea name="note" rows="2" style="height: auto !important" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="button" id="btn-save-sale"><i class="fas fa-plus mr-1"></i> Simpan Transaksi</button>
                                    <button class="btn btn-info" type="button" id="btn-print-sale" data-id="0"><i class="fa fa-print mr-1"></i> Cetak Transaksi</button>
                                    <button class="btn btn-danger" type="button" id="btn-reset-sale"><i class="fa fa-redo mr-1"></i> Reset Transaksi</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Add item-->
<div class="modal fade" id="modal-add-item" tabindex="-1" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute;right: 0;margin-right: 15px;margin-top: 13px;z-index: 1;width: 32px;">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body">
                <table class="table table-striped " id="table-item" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">No.</th>
                            <th></th>
                            <th>Nama Item</th>
                            <th>Kode</th>
                            <th>Inventory</th>
                            <th class="text-right">Harga</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambahkan</button>
            </div> --}}
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('assets/bundles/input-spinner/bootstrap-input-spinner.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/datatables.js') }}"></script>
{{-- <script>
    $("input[type='number']").inputSpinner();
</script> --}}
<script>
    $(document).ready(function () {
        let items = $('#table-item').DataTable( {
            "processing": true,
            "scrollY"   :"500px",
            "scrollCollapse": true,
            "paging":   false,
            "ordering": false,
            "info":     false,
            "ajax": "{!! url()->current() !!}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        return `<img alt="image" src="`+row.image+`" height="25px">`;
                    },"bSortable": false
                },
                { "data": "name"},
                { "data": "code"},
                { "data": "inventory", "sClass": "text-right" },
                { "data": "price", "sClass": "text-right"},
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        return `<button class="btn btn-sm btn-warning btn-add-item" data-id="`+row.id+`"><i class="fa fa-plus"></i> </button>`;
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                $("#table-item_filter").parent().prev().html(`<h5 class="modal-title" style="font-size: 1.15rem;">Tambah Item Pembelian</h5>`);
                $("#table-item_filter").parent().prev().addClass('col-12 col-lg-6').removeClass('col-sm-12 col-md-6')
                $("#table-item_filter").parent().addClass('col-12 col-lg-6').removeClass('col-sm-12 col-md-6')
                $("#table-item_filter").css('text-align', 'center');
                $(".table-striped").removeClass('no-footer');
            },
            "createdRow": function (row, data, index) {
                // $('td', row).eq(2).css('width', '40%');
            },
        });
        items.on( 'order.dt search.dt', function () {
            items.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#modal-add-item').on('shown.bs.modal', function (e) {
            items.columns.adjust();
        });

        // setTimeout(function () {
        //     // modal-add-sale
        //     items.DataTable().columns.adjust().draw();
        //     // items.columns.adjust().draw();
        //     // alert('safsafsa');
        // },2000);
    });

    var arr_item_id = [];
    var index_item = 0;

    $('#modal-add-item').on('click','.btn-add-item', function (e) { 
        e.preventDefault();
        let id = $(this).data("id");
        let el = $(this).parent().parent().children();
        let inventory = el.eq(4).html();
        
        if (inventory == 0) {
            alert('Barang Sekarang Sedang Kosong');
        } else if (arr_item_id.indexOf(id) == '-1') {
            let name = el.eq(2).html();
            let code = el.eq(3).html();
            let price = el.eq(5).html();
            arr_item_id.push(id); 
            $('#table-sale tbody tr:last').before(`
                <tr>
                    <td scope="row">1</td>
                    <td class="align-middle">`+name+`</td>
                    <td>`+code+`</td>
                    <td>Rp. `+price+`</td>
                    <td class="text-center" style="width: 166px">
                        <input type="hidden" name="items[`+index_item+`][id]" value="`+id+`" form="form-sale" />
                        <input class="input-qty-item" type="number" value="1" min="1" name="items[`+index_item+`][qty]" max="`+inventory+`" form="form-sale"/>
                    </td>
                    <td class="text-right">Rp. `+price+`</td>
                    <td class="text-center">
                        <a href="#" onclick="return false;" class="item-delete" style="color: red" data-id="`+id+`"><i class="fa fa-close"></i> Hapus</a>
                    </td>
                </tr>
            `);
            $('#table-sale tbody tr:last').prev('tr').children().eq(4).children().last().inputSpinner();
            index_item++;
            $('#modal-add-item').modal('hide');
            refreshItem()
        } else {
            alert('item sudah ditambahkan sebelumnya.')
        }
    });

    $('#table-sale tbody').on('change','.input-qty-item', function (e) { 
        e.preventDefault();
        let qty = $(this).val();
        let price = $(this).parent().prev().html().split(' ').pop();
        let result = qty*price;
        $(this).parent().next().html('Rp. '+result);
        newTotalPrice();
    });

    $('#table-sale tbody').on('click','.item-delete', function (e) { 
        e.preventDefault()
        let id = $(this).data("id");
        
        arr_item_id = jQuery.grep(arr_item_id, function(value) {
            return value != id;
        });
        $(this).parent().parent().remove();
        refreshItem();
    });

    function refreshItem() {
        numberingItem();
        newTotalPrice();
    }

    function numberingItem() {
        let count = 1;
        $('#table-sale tbody tr:not(:last-child)').each(function() {
            $(this).find("td:first").html(count);
            count++;
        });
    };

    function newTotalPrice() {
        let amount = 0;
        $('#table-sale tbody tr:not(:last-child)').each(function() {
            amount += ~~$(this).find('td').eq(5).html().split(' ').pop();
        });
        $('#form-sale [name="total_price"]').val(amount);
        let discount = ~~$('#form-sale [name="discount"]').val();
        let total = amount - discount;
        $('h3').html('Total <span class="mr-3">:</span>'+formatRupiah(total)+',00');
        $('#form-sale [name="final_amount"]').val(total);

        let paid_amount = $('#form-sale [name="paid_amount"]').val();
        if (paid_amount != 0) {
            $('#form-sale [name="change"]').val(paid_amount - total);
        } else {
            $('#form-sale [name="change"]').val(0);
        }
    }

    $('#form-sale').on('change keyup', '[name="discount"],[name="paid_amount"]', function () {
        newTotalPrice();
    })

    $('#btn-save-sale').click(function (e) { 
        e.preventDefault();

        if (arr_item_id.length == 0) {
            showAlert('error', 'Pilih produk yang dibeli', 'Transaksi Gagal');
            return false
        }

        clearError('form-sale');
        let btn = $(this);
        
        $.ajax({
            type: "POST",
            url: "{{ route('transaction.store') }}",
            data: $('#form-sale').serialize(),
            dataType: "json",
            beforeSend: function() {
                btn.addClass('btn-progress');
            },
            success: function (response) {
                if (response.status) {
                    showAlert('success', response.message, 'Transaksi Produk');
                    $('#btn-print-sale').attr('data-id',response.data.id);
                    $('#btn-print-sale').click();
                } else {
                    for (let key of Object.keys(response.errors)) {
                        $('[name="'+key+'"]').addClass('is-invalid');
                        $('[name="'+key+'"]').after('<div class="invalid-feedback">'+response.errors[key]+'.</div>');
                    }
                        showAlert('error', 'Silakan periksa formulir', 'Transaksi Gagal')
                }
                btn.removeClass('btn-progress');
            }, 
            error: function(xhr, status, error) {
                btn.removeClass('btn-progress');
                showAlert('error', 'Terjadi Kesalahan', 'Transaksi Produk')
                console.log(xhr.responseText);
            }
        });
    });

    $('#btn-print-sale').click(function (e) {
        let sale_id = $(this).attr("data-id");
        let url_print = "{{ route('transaction.show', ':id') }}";

        if (sale_id == 0) {
            alert('Transaksi tidak ditemukan.');
            return false;
        }

        $.ajax({
            type: "GET",
            url: url_print.replace(':id', sale_id),
            dataType: "json",
            success: function (response) {
                if (response.status)
                    popupCenter({url: url_print.replace(':id', sale_id), title: 'xtf', w: 700, h: 440}); 
                else 
                    alert('Transaksi tidak ditemukan.')
            }
        });
    });

    $('#btn-reset-sale').click(function (e) { 
        $('#btn-print-sale').attr('data-id',0);
        $('#form-sale input').val('');
        $('#form-sale tbody').html(`
            <tr>
                <td scope="row"></td>
                <td colspan="6"><button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-add-item">Tambah Item</button></td>
            </tr>
        `);
        
        arr_item_id = [];
        index_item = 0;

        refreshItem();
    });

    const popupCenter = ({url, title, w, h}) => {
        // Fixes dual-screen position                             Most browsers      Firefox
        const dualScreenLeft = window.screenLeft !==  undefined ? window.screenLeft : window.screenX;
        const dualScreenTop = window.screenTop !==  undefined   ? window.screenTop  : window.screenY;

        const width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        const height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        const systemZoom = width / window.screen.availWidth;
        const left = (width - w) / 2 / systemZoom + dualScreenLeft
        const top = (height - h) / 2 / systemZoom + dualScreenTop
        const newWindow = window.open(url, title, 
        `
        scrollbars=yes,
        width=${w / systemZoom}, 
        height=${h / systemZoom}, 
        top=${top}, 
        left=${left}
        `
        )

        if (window.focus) newWindow.focus();
    }
</script>
@endpush