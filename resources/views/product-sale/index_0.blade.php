@extends('layout.main')
@section('title','Riwayat Penjualan Produk')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card card-warning">
                {{-- <div class="card-header">
                    <h4>Basic DataTables</h4>
                </div> --}}
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-sales" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th>Pembeli</th>
                                    <th>Jumlah Harga</th>
                                    <th style="font-weight: 900"> Total Harga & ID</th>
                                    <th>Pembayaran</th>
                                    <th>Metode</th>
                                    <th class="text-center">Status</th>
                                    <th>Produk</th>
                                    <th class="text-center">Tanggal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Synchronize Data Server -->
<div class="modal fade" id="modal-sync-server" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body pt-4">
                <h6>Apakah anda yakin ingin melakukan sinkronisasi terhadap data server ?</h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-warning" id="btn-sync-server">Ya sinkronkan data ke server</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Delete-->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus Riwayat ini ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="btn-delete-sale">Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="assets/bundles/datatables/datatables.min.css">
<link rel="stylesheet" href="assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="assets/bundles/datatables/datatables.min.js"></script>
<script src="assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<!-- Page Specific JS File -->
<script src="assets/js/page/datatables.js"></script>

<script>
    var sale_id = 1;

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        })
        let dt_sales = $('#table-sales').DataTable( {
            "processing": true,
            "ajax": "{!! url()->current() !!}",
            "columns": [
                {
                    "data": null,
                    "width": "10px",
                    "sClass": "text-center",
                    "bSortable": false
                },
                { "data": "null", render: function ( data, type, row ) {
                        return '<p class="mb-0">'+row.cust_name+'</p><small>'+row.user.name+'</small>'
                    }
                },
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        return '<p class="mb-1">'+formatRupiah(row.total_price)+'</p><small>- '+formatRupiah(row.discount)+'</small>';
                    }
                },
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        return '<h6 class="mb-0"><b>'+formatRupiah((row.final_amount))+'</b></h6><small>'+row.id+'</small>';
                    }
                },
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        return '<p class="mb-0">'+formatRupiah(row.paid_amount)+'</p><small> '+formatRupiah(row.change)+'</small>';
                    }
                },
                { "data": "payment.name"},
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        return row.is_completed != 0 ? '<span class="badge badge-sm badge-success"> Selesai </span>' : '<span class="badge badge-sm badge-secondary"> Pending </span>';
                    }
                },
                { "data": "null", "sClass": "text-right", render: function ( data, type, row ) {
                        let html = '<small>';
                        $.each(row.items, function (index, value) { 
                            html += value.qty+' x '+value.name+' @ '+value.price+'<br>'
                        });
                        return html += '</small>';
                    }
                },
                { "data": "created_at", "sClass": "text-center" },
                { "data": "null", "sClass": "text-center", render: function ( data, type, row ) {
                        let url_detail = "{{ route('product-sales.show', ':id') }}";
                        return `
                            <a href="`+url_detail.replace(':id', row.id)+`" class="btn btn-sm btn-info text-white" title="detail"><i class="fas fa-pencil-alt mr-1"></i> Detail</a>
                            `;
                    },"bSortable": false
                }
            ],
            'initComplete': function(){
                if ("{{ config('app.env') }}" != "local") {
                    $("#table-sales_filter").parent().after(`
                        <div class="col-12 col-md-4 text-center text-md-right">
                            <button class="btn btn-rounded btn-warning mb-2" data-toggle="modal" data-target="#modal-sync-server"><i class="fa fa-redo mr-2"></i> Sinkronkan Data ke Server Hosting</a>
                        </div>
                    `);
                }
                $("#table-sales_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
                $("#table-sales_filter").css('text-align', 'center');
                $("#table-sales_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
            }  
        });
        dt_sales.on( 'order.dt search.dt', function () {
            dt_sales.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#btn-delete-sale').click(function (e) { 
            e.preventDefault();
            let url_delete = "{{ route('ticket-sales.destroy', ':id') }}";

            $.ajax({
                type: "DELETE",
                url: url_delete.replace(':id', sale_id),
                success: function (response) {
                    if (response.status) {
                        dt_sales.ajax.reload(function () {
                            $('#modal-delete').modal('hide');
                            showAlert('success',response.message,'Berhasil')
                        }, false);
                    }
                },
            });
        });
    });

    function importTickets() {
        $('#input-file-import').click();
    }

    $('#input-file-import').on('change', function(){ 
        if ($(this).get(0).files.length != 0) {
            $( "#form-import" ).submit();
        }
    });

    function deleteSale(id) {
        sale_id = id;
        $('#modal-delete').modal('show');
    }

    $('#btn-sync-server').click(function (e) { 
        e.preventDefault();
        let btn = $(this)

        $.ajax({
            type: "GET",
            url: "{{ route('transaction.synchronized-data') }}",
            dataType: "json",
            beforeSend: function() {
                btn.addClass('btn-progress');
            },
            success: function (response) {
                console.log(response);
                if (response.status) {
                    $('#modal-sync-server').modal('hide');
                    showAlert('success',response.message,'Berhasil')
                } else {
                    showAlert('error',response.message,'Gagal')
                }
                btn.removeClass('btn-progress')
            },
        });
    });
</script>
@endpush