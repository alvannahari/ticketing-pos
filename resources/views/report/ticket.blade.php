@extends('layout.main')
@section('title','Laporan Sistem Tiket')

@section('content')
<section class="section">
    @include('report.filter', ['report' => 'ticket'])
    <div class="card card-warning">
        <div class="card-header">
            <h4>Data Penjualan</h4>
        </div>
        <div class="card-body pt-1">
            <div class="table-responsive" >
                <table class="table table-md table-striped" id="table-data" style="width: 100%">
                    <thead class="text-center bg-light">
                        <tr>
                            <th class="text-dark">No.</th>
                            <th class="text-dark">ID. Transaksi</th>
                            <th class="text-dark">Tanggal</th>
                            <th class="text-dark">Jenis Tiket</th>
                            <th class="text-dark">Metode Pembayaran</th>
                            <th class="text-dark">QTY</th>
                            <th class="text-dark">Harga</th>
                            <th class="text-dark">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
@endsection

@push('addons-style')
<link rel="stylesheet" href="assets/bundles/datatables/datatables.min.css">
<link rel="stylesheet" href="assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="assets/bundles/datatables/datatables.min.js"></script>
<script src="assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<!-- Page Specific JS File -->
<script src="assets/js/page/datatables.js"></script>
<script>
    $(document).ready(function () {
        loadData();
    })

    function renderElData(data) {
        let html = ''
        let qty = 0
        let total = 0

        data.forEach((value, index) => {
            qty += parseInt(value.qty)
            total += parseInt(value.qty * value.price)

            html += `
                <tr>
                    <td>`+(index + 1)+`</td>
                    <td>`+value.sale.id+`</td>
                    <td>`+moment(value.created_at).format('DD/MM/YYYY')+`</td>
                    <td class="text-left">`+value.name+`</td>
                    <td>`+value.sale.payment.name+`</td>
                    <td>`+value.qty+`</td>
                    <td class="text-right">`+ formatRupiah(value.price)+`</td>
                    <td class="text-right">`+ formatRupiah(value.qty * value.price)+`</td>
                </tr>
            `
        });

        html += `
            <tr>
                <td colspan="5" class="font-bold text-center bg-light">TOTAL</td>
                <td class="font-bold text-center bg-light">`+qty+`</td>
                <td colspan="2" class="font-bold text-right bg-light">`+formatRupiah(total)+`</td>
            </tr>
        `

        $('#table-data tbody').html(html);
    }
</script>
@endpush