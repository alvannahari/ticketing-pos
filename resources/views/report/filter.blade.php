<div class="card card-info">
    <div class="card-header">
        <h4>Filter Laporan</h4>
    </div>
    <div class="card-body pt-1">
        <div class="row">
            <div class="col-12 col-lg-5">
                <label for="">Pilih Rentang Waktu</label>
                <div id="reportrange" class="form-control">
                    <i class="fa fa-calendar mr-3"></i>
                    <span></span> <i class="fa fa-caret-down" style="float:right;"></i>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <form action="{{ route('report.export.tickets') }}" method="POST" id="form-export">
                    @csrf
                    <input type="hidden" name="start">
                    <input type="hidden" name="end">
                    <input type="hidden" name="admin_id">
                </form>
                <div class="row pt-4">
                    <div class="col-6 pt-1">
                        <select name="admin_id" class="form-control">
                            <option value="0">Semua</option>
                            @foreach ($admins as $admin)
                                <option value="{{ $admin->id }}">{{ $admin->name }}</option>
                            @endforeach
                        </select>
                        {{-- <button class="btn btn-warning w-100 py-2" id="btn-filter"><i class="fas fa-filter mr-2"></i> Filter Data</button> --}}
                    </div>
                    <div class="col-6 pt-1">
                        <button class="btn btn-success w-100 py-2" id="btn-export"><i class="fas fa-file-export mr-2"></i> Export Data</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('addons-style')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    #reportrange {
        box-shadow: 0px 0px 0px 0px #e0e0e0;
    }
    #reportrange:hover {
        cursor: pointer;
        box-shadow: 0px 0px 0px 2px #e0e0e0;
    }
</style>
@endpush

@push('addons-script')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    var start = moment().startOf('month');
    var end = moment();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    })

    $('#reportrange').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
            'Hari Ini': [moment(), moment()],
            // 'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            // '7 Hari Terkahir': [moment().subtract(6, 'days'), moment()],
            // '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
            'Minggu Ini': [moment().startOf('week').add(1, "days"), moment().endOf('week').add(1, "days")],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
            'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, callbackDateRange);

    callbackDateRange(start, end, false);

    // $('#btn-filter').click(function (e) { 
    //     e.preventDefault();
    //     start = moment($('#reportrange').data('daterangepicker').startDate);
    //     end = moment($('#reportrange').data('daterangepicker').endDate);
    //     clearData();
    //     loadData();
    //     showAlert('success', 'Filter data berhasil diterapkan.', 'Berhasil')
    // });

    $('#btn-export').click(function (e) { 
        e.preventDefault();
        
        let url = "{!! route('report.export.tickets') !!}"
        if ("{{ $report }}" == "store")
            url = "{!! route('report.export.store') !!}"

        $('#form-export').attr('action', url);
        $('#form-export input[name="start"]').val(start.format('YYYY-MM-DD'));
        $('#form-export input[name="end"]').val(end.format('YYYY-MM-DD'));
        $('#form-export input[name="admin_id"]').val($('select[name="admin_id"]').val());

        $('#form-export').submit();

        setTimeout(() => {
            showAlert('success', 'Export data berhasil dilakukan.', 'Berhasil')
        }, 500);

        
        // $.ajax({
        //     type: "POST",
        //     url: url,
        //     data: {
        //         "start" : start.format('YYYY-MM-DD'),
        //         "end" : end.format('YYYY-MM-DD'),
        //     },
        //     dataType: "json",
        //     success: function (response) {
        //         showAlert('success', response.message, 'Berhasil')
        //     }
        // });
    });

    $('select[name="admin_id"]').change(function (e) { 
        e.preventDefault();
        filterData(true);
    });

    function callbackDateRange(start, end, notif = true) {
        $('#reportrange span').html(start.format('DD MMMM YYYY') + ' <span style="font-weight:900;margin: 0px 6px;"> - </span> ' + end.format('DD MMMM YYYY'));
        filterData(notif);
    }

    function filterData(notif) {
        start = moment($('#reportrange').data('daterangepicker').startDate);
        end = moment($('#reportrange').data('daterangepicker').endDate);
        clearData();
        loadData();
        if (notif) showAlert('success', 'Filter data berhasil diterapkan.', 'Berhasil')
    }

    function clearData() {
        $('#table-data tbody').html('');
    }

    function loadData() {
        $.ajax({
            type: "get",
            url: "{!! url()->current() !!}",
            data: {
                "start" : start.format('YYYY-MM-DD'),
                "end" : end.format('YYYY-MM-DD'),
                "admin_id" : $('select[name="admin_id"]').val(),
            },
            dataType: "json",
            beforeSend : function () {
                $('#btn-filter,#btn-export').addClass('btn-progress');
            },
            success: function (response) {
                // console.log(response);
                renderElData(response.data)
                $('#btn-filter,#btn-export').removeClass('btn-progress')
            }
        });
    }
</script>
@endpush