@extends('layout.main')
@section('title', 'Informasi Detail Penjualan Tiket')

@section('content')
<div class="row">
    <div class="col-12 col-lg-5">
        <div class="card">
            <div class="card-header">
                <h4>Informasi Penjualan</h4>
            </div>
            <div class="card-body" style="height: 700px;">
                <table class="table w-100">
                    <tbody>
                        <tr>
                            <td>ID Transaksi</td>
                            <td>:</td>
                            <td>{{ $sale->id }}</td>
                        </tr>
                        <tr>
                            <td>Nama Pengunjung</td>
                            <td>:</td>
                            <td>{{ $sale->cust_name }}</td>
                        </tr>
                        <tr>
                            <td>Jenis Pembayaran</td>
                            <td>:</td>
                            <td>{{ $sale->payment->name }}</td>
                        </tr>
                        <tr>
                            <td>Petugas Loket</td>
                            <td>:</td>
                            <td>{{ $sale->user->name }}</td>
                        </tr>
                        <tr>
                            <td>Jumlah Tiket</td>
                            <td>:</td>
                            <td>{{ $sale->items_count }}</td>
                        </tr>
                        <tr>
                            <td>Total Harga</td>
                            <td>:</td>
                            <td>Rp. {{ $sale->total_price }}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>:</td>
                            <td>
                                @if ($sale->is_completed)
                                    <span class="badge badge-sm badge-success">Selesai</span>
                                @else
                                    <span class="badge badge-sm badge-warning">Pending</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Catatan</td>
                            <td>:</td>
                            <td>{{ $sale->note }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal</td>
                            <td>:</td>
                            <td>{{ $sale->created_at }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-12 col-lg-7">
        <div class="card">
            <div class="card-header">
                <h4>Informasi Tiket.</h4>
            </div>
            <div class="card-body" style="height: 700px;overflow-y: scroll;">
                @foreach ($sale->items as $item)
                    <div class="row py-2">
                        <div class="col-12 col-lg-6" style="white-space: nowrap;">
                            <span class="mr-3"><b>{{ $loop->iteration }}</b></span>
                            @if (str_contains($item->type, ' Domestik'))
                                <img src="{{ asset('assets/img/ticket/tiket_domestik.jpg') }}" alt="" srcset="" style="width:90%;max-width:360px">
                            @elseif (str_contains($item->type, ' Nasional'))
                                <img src="{{ asset('assets/img/ticket/tiket_nasional.jpg') }}" alt="" srcset="" style="width:90%;max-width:360px">
                            @else 
                                <img src="{{ asset('assets/img/ticket/tiket_internasional.jpg') }}" alt="" srcset="" style="width:90%;max-width:360px">
                            @endif
                        </div>
                        <div class="col-12 col-lg-6 pl-lg-0 pt-3 pt-lg-4 text-center text-lg-left">
                            <h6>{{ $item->type }}</h6>
                            <p class="my-0" style="color: #6c757d"><b>Kode : {{ $item->ticket->barcode }}</b></p>
                            <p class="my-0" style="color: #6c757d"><b>Harga : {{ $item->price }}</b></p>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection