@extends('layout.main')

@section('title', 'Tambah Produk Baru')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12 col-lg-10">
            <div class="card">
                <form action="{{ route('products.store') }}" method="POST" id="form-product" enctype="multipart/form-data">
                    <div class="card-body pb-1">
                        @csrf
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Nama Produk</label>
                                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" placeholder=". . . ." >
                                    @error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Kode Produk</label>
                                    <input type="text" name="code" class="form-control @error('code') is-invalid @enderror" placeholder=". . . .">
                                    @error('code') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="desc" rows="4" style="height: auto !important" class="form-control"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Harga</label>
                                    <input type="number" name="price" class="form-control @error('price') is-invalid @enderror" required value="0" min="0">
                                    @error('price') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Inventory</label>
                                    <input type="number" name="inventory" class="form-control @error('inventory') is-invalid @enderror" required value="0" min="0">
                                    @error('inventory') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-group">
                                    <label>Gambar</label>
                                    <img class="d-block mx-2 mb-2 mt-0 file-image" src="{{ asset('assets/img/not_found.png') }}" alt="" style="height: 140px;">
                                    <input type="file" name="image" class="form-control pt-2 @error('image') is-invalid @enderror" accept="image/*">
                                    @error('image') <div class="invalid-feedback">{{ $message }}</div> @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer pt-0">
                        <button class="btn btn-warning" type="submit"><i class="fas fa-plus mr-2"></i> Tambahkan Produk Baru</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection

@push('addons-style')
<style>
    input[type="file"]:hover {
        cursor: pointer;
    }
    .file-image{
        max-width:100%;
        filter: drop-shadow(0px 2px 6px #636262);
    }
    .file-image:hover{
        cursor: pointer;
        filter: drop-shadow(0px 2px 6px #333333);
    }
</style>
@endpush

@push('addons-script')
<script>
    let image = "{{ asset('assets/img/not_found.png') }}";
    $(document).ready(function () {
        $('.file-image').click(function (e) { 
            e.preventDefault();
            $('input[name="image"]').click();
        });
    });

    $('input[name="image"]').on("change", function() {
        if ($(this).val() == '') {
            $('.file-image').attr("src",image);
        } else {
            let reader = new FileReader();
            reader.onload = (e) => { 
                $('.file-image').attr("src",e.target.result);
            }
            reader.readAsDataURL(this.files[0]); 
        }
    });
</script>
@endpush