@extends('layout.main')
@section('title','Daftar Produk')

@section('content')
<section class="section">
    <div class="row">
        <div class="col-12">
            <div class="card card-warning">
                {{-- <div class="card-header">
                    <h4>Basic DataTables</h4>
                </div> --}}
                <form action="{{ route('export.products') }}" method="POST" id="form-export-product" style="display: none">
                    @csrf
                </form>
                <div class="card-body">
                    <div class="table-responsive" >
                        <table class="table table-striped" id="table-products" style="width: 100%">
                            <thead>
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th></th>
                                    <th>Nama</th>
                                    <th>Kode</th>
                                    <th>Inventory</th>
                                    <th>Harga @</th>
                                    <th class="text-center">Created At</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Modal Print -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-info" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus Tiket ini ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="btn-delete-products">Hapus</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Delete -->
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Konfirmasi Hapus</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus Produk ini ?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger" id="btn-delete-products">Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('addons-style')
<link rel="stylesheet" href="assets/bundles/datatables/datatables.min.css">
<link rel="stylesheet" href="assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css">
<style>
    table.dataTable img {
        -webkit-box-shadow: 0 5px 15px 0 rgba(105,103,103,0.5);
        box-shadow: 0 5px 15px 0 rgba(105,103,103,0.5);
        border: 0px solid #ffffff;
        border-radius: 2px;
    }
    .btn-purple {
        box-shadow: 0 2px 6px #7a35f8;
        background-color: #6d31dd;
        border-color: #6d31dd;
        color: white;
    }
    .btn-purple:hover,.btn-purple:active,.btn-purple:focus {
        background-color: #7a35f8 !important;
        color: white;
    }
</style>
@endpush

@push('addons-script')
<!-- JS Libraies -->
<script src="assets/bundles/datatables/datatables.min.js"></script>
<script src="assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/bundles/jquery-ui/jquery-ui.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.13.5/sorting/natural.js"></script>
<!-- Page Specific JS File -->
<script src="assets/js/page/datatables.js"></script>

<script>
    var product_id = 1;

    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            }
        })
        let dt_products = $('#table-products').DataTable( {
            processing: true,
            ajax: "{!! url()->current() !!}",
            order: [[6, 'desc']],
            columns: [
                { data: null, width: "10px", sClass: "text-center", bSortable: false },
                { data: "null", sClass: "text-center", render: function ( data, type, row ) {
                        return `<img alt="image" src="`+row.image+`" height="45px">`;
                    },bSortable: false
                },
                { data: "name" },
                { data: "code" },
                { data: "inventory", sClass: "text-right" },
                { data: "null", type: 'natural', sClass: "text-right", render: function ( data, type, row ) {
                        return formatRupiah(row.price);
                    }
                },
                { data: "created_at", sClass: "text-center" },
                { data: "null", sClass: "text-center", render: function ( data, type, row ) {
                        let url_detail = "{{ route('products.show', ':id') }}";
                        return `
                            <a href="`+url_detail.replace(':id', row.id)+`" class="btn btn-sm btn-success" title="update" style="color:white"><i class="fas fa-pencil-alt mr-1"></i> Update</a>
                            <button class="btn btn-sm btn-info" onclick="printBarcode('`+row.code+`')"><i class="fas fa-barcode mr-1"></i> Cetak</button>
                            <button class="btn btn-sm btn-danger" onclick="deleteProduct('`+row.id+`')"><i class="fa fa-trash mr-1"></i> Hapus</button>
                        `;
                    },bSortable: false
                }
            ],
            initComplete: function(){
                $("#table-products_filter").parent().after(`
                    <div class="col-12 col-md-4 text-center text-md-right">
                        <button class="btn btn-rounded btn-purple mb-2" id="btn-export"><i class="fa fa-print mr-2"></i> Export Data</button>
                        <a href="{{ route('products.create') }}" class="btn btn-rounded btn-warning mb-2" style="color:white"><i class="fa fa-plus mr-2"></i> Tambahkan Produk Baru</a>
                    </div>
                `);
                $("#table-products_length").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
                $("#table-products_filter").css('text-align', 'center');
                $("#table-products_filter").parent().addClass('col-12 col-sm-6 col-md-4').removeClass('col-sm-12 col-md-6')
            }   
        });
        dt_products.on( 'order.dt search.dt', function () {
            dt_products.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#btn-delete-products').click(function (e) { 
            e.preventDefault();
            let url_delete = "{{ route('products.destroy', ':id') }}";

            $.ajax({
                type: "DELETE",
                url: url_delete.replace(':id', product_id),
                success: function (response) {
                    if (response.status) {
                        dt_products.ajax.reload(function () {
                            $('#modal-delete').modal('hide');
                            showAlert('success',response.message,'Berhasil')
                        }, false);
                    }
                },
            });
        });

        $('#table-products_wrapper').on('click', 'button#btn-export', function () {
            $('#form-export-product').submit();
        });
    });

    function deleteProduct(id) {
        product_id = id;
        $('#modal-delete').modal('show');
    }

    function printBarcode(code) {
        let url_print = "{{ route('product.print-barcode', ':id') }}";

        popupCenter({url: url_print.replace(':id', code), title: 'xtf', w: 700, h: 440}); 
    }
</script>
@endpush