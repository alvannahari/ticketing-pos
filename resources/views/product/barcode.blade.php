<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Barcode</title>
    <link href='{{ asset('assets/css/font-family-inter.css') }}' rel='stylesheet'>
    <style>
        body {
            font-family: 'Inter';
            text-align:center;
            margin-top: 6px;
            margin-bottom: 0px;
        }
        #price {
            margin-top: 0px;
            margin-bottom: 5px;
            font-size:12px;
            font-weight: bold;
        }
        #code {
            margin-top: -5px;
            font-size:8px;
            letter-spacing: 5px;
        }
    </style>
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            window.print();
            setTimeout(() => {
                window.close()
            }, 500);

        });
    </script>
</head>
<body>
    <p id="price"> Rp. {{ number_format($product->price,0,'','.') }} </p>

    @php
        $generatorPNG = new Picqer\Barcode\BarcodeGeneratorPNG();
    @endphp

    <img src="data:image/png;base64,{{ base64_encode($generatorPNG->getBarcode($product->code, $generatorPNG::TYPE_CODE_128, 1,20)) }}">

    <p id="code" >{{ $product->code }}</p>
</body>
</html>