<?php

namespace App\Providers;

use App\Models\Product;
use App\Models\ProductSale;
use App\Models\TicketSale;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void {
        TicketSale::creating(function ($sale) {
            if ( ! $sale->id)
                $sale->id = TicketSale::generateTicketId();
        });
        Product::creating(function ($product) {
            if ( ! $product->id)
                $product->id = Product::generateProductId();
        });
        ProductSale::creating(function ($sale) {
            if ( ! $sale->id)
                $sale->id = ProductSale::generateProductSaleId();
        });
    }
}
