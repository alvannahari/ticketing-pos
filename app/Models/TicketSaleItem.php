<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketSaleItem extends Model {

    use HasFactory;

    const ID = 'id';
    const SALE_ID = 'sale_id';
    const TICKET_TYPE_ID = 'ticket_type_id';
    const NAME = 'name';
    const PRICE = 'price';
    const QTY = 'qty';
    const AMOUNT = 'amount';

    protected $guarded = [];

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function sale() {
        return $this->belongsTo(TicketSale::class, Self::SALE_ID);
    }

    function type() {
        return $this->belongsTo(TicketType::class);
    }
}
