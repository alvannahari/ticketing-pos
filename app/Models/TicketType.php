<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketType extends Model {

    use HasFactory;

    const IMAGE_PATH = 'ticket/';

    const ID = 'id';
    const NAME = 'name';
    const PRICE = 'price';
    const IMAGE = 'image';
    const INVENTORY = 'inventory';

    const TICKET_FREE = 1;
    const TICKET_DOMESTIC = 2;
    const TICKET_NATIONAL = 3;
    const TICKET_INTERNATIONAL = 4;

    protected $guarded = [];

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function tickets() {
        return $this->hasMany(Ticket::class, Ticket::TYPE_ID);
    }

    function getImageAttribute($value) {
        return asset('assets/img/'.Self::IMAGE_PATH.$value);
    }

    function decrementInventory($qty = 1) {
        $inventory = $this->attributes[Self::INVENTORY] - $qty;
        $this->update([Self::INVENTORY => $inventory]);
        return;
    }

    function incrementInventory($qty = 1) {
        $inventory = $this->attributes[Self::INVENTORY] + $qty;
        $this->update([Self::INVENTORY => $inventory]);
        return;
    }
}
