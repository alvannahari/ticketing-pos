<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketSale extends Model {

    use HasFactory;

    const ID = 'id';
    const USER_ID = 'user_id';
    const PAYMENT_ID = 'type_payment_id';
    const CUST_NAME = 'cust_name';
    const TOTAL_PRICE = 'total_price';
    const DISCOUNT = 'discount';
    const FINAL_AMOUNT = 'final_amount';
    const PAID_AMOUNT = 'paid_amount';
    const CHANGE = 'change';
    const NOTE = 'note';
    const IS_COMPLETED = 'is_completed';

    protected $guarded = [];
    protected $primaryKey = Self::ID;
    public $incrementing = false;
    protected $keyType = 'string';

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function user() {
        return $this->belongsTo(User::class)->withTrashed();
    }

    function items() {
        return $this->hasMany(TicketSaleItem::class, TicketSaleItem::SALE_ID);
    }

    function payment() {
        return $this->belongsTo(TypePayment::class, Self::PAYMENT_ID);
    }

    function scopeGenerateTicketId() {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $id = 'SBY*ANI-';

        for ($i = 0; $i < 12; $i++) 
            $id .= $characters[random_int(0, strlen($characters) - 1)];

        if ($this->ticketIdExists($id)) return $this->scopeGenerateTicketId();

        return $id;
    }

    function ticketIdExists($id) {
        return $this->where(Self::ID, $id)->exists();
    }
}
