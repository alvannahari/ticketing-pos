<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypePayment extends Model {

    use HasFactory;

    const ID = 'id';
    const NAME = 'name';
    const IS_ACTIVE = 'is_active';

    protected $guarded = [];

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function ticketSales() {
        return $this->hasMany(TicketSale::class, TicketSale::PAYMENT_ID);
    }

    function productSales() {
        return $this->hasMany(ProductSale::class, ProductSale::PAYMENT_ID);
    }
}
