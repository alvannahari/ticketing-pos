<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    use HasFactory;

    const IMAGE_PATH = 'product/';

    const ID = 'id';
    const NAME = 'name';
    const CODE = 'code';
    const INVENTORY = 'inventory';
    const PRICE = 'price';
    const CAPITAL_PRICE = 'capital_price';
    const IMAGE = 'image';
    const DESC = 'desc';

    protected $guarded = [];
    protected $primaryKey = Self::ID;
    public $incrementing = false;
    protected $keyType = 'string';

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function saleItems() {
        return $this->hasMany(ProductSale::class);
    }

    function scopeGenerateProductId() {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $id = 'PRODUCT-';

        for ($i = 0; $i < 16; $i++) 
            $id .= $characters[random_int(0, strlen($characters) - 1)];

        if ($this->productIdExists($id)) return $this->scopeGenerateProductId();

        return $id;
    }

    function productIdExists($id) {
        return $this->where(Self::ID, $id)->exists();
    }

    function getImageAttribute($value) {
        if ($value == null) return asset('assets/img/not_found.png');
        else return asset('storage/'.Self::IMAGE_PATH.$value);
    }

    function decrementStok($qty = 1) {
        $inventory = $this->attributes[Self::INVENTORY] - $qty;
        $this->update([Self::INVENTORY => $inventory]);
        return;
    }

    function incrementStok($qty = 1) {
        $inventory = $this->attributes[Self::INVENTORY] + $qty;
        $this->update([Self::INVENTORY => $inventory]);
        return;
    }
}
