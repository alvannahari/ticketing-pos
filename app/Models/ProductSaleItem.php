<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSaleItem extends Model {

    use HasFactory;

    const ID = 'id';
    const SALE_ID = 'sale_id';
    const PRODUCT_ID = 'product_id';
    const NAME = 'name';
    const QTY = 'qty';
    const CODE = 'code';
    const PRICE = 'price';
    const CAPITAL_PRICE = 'capital_price';
    const PROFIT = 'profit';
    const DESC = 'desc';

    protected $guarded = [];

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function sale() {
        return $this->belongsTo(ProductSale::class);
    }

    function product() {
        return $this->belongsTo(Product::class);
    }
}
