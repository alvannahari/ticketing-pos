<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model {

    use HasFactory;

    const ID = 'id';
    const USER_ID = 'user_id';
    const LOG_NAME = 'log_name';
    const DEVICE = 'device';
    const ROLE = 'role';
    const DATA = 'data';
    const DESC = 'desc';

    const UPDATED_AT = null;

    protected $guarded = [];

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function user() {
        return $this->belongsTo(User::class)->withTrashed();
    }
}
