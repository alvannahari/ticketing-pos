<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {

    use HasApiTokens, HasFactory, Notifiable, HasRoles, SoftDeletes;

    const ID = 'id';
    const USERNAME = 'username';
    const PASSWORD = 'password';
    const NAME = 'name';
    const ROLE_ID = 'role_id';

    const ROLE_OWNER = '1';
    const ROLE_TICKET = '2';
    const ROLE_POS = '3';

    protected $guarded = [];

    protected $hidden = [Self::PASSWORD, 'deleted_at'];

    protected $casts = [Self::PASSWORD => 'hashed'];

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function sales() {
        return $this->hasMany(TicketSale::class);
    }

    function activities() {
        return $this->hasMany(LogActivity::class);
    }
}
