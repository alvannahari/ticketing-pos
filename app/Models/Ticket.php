<?php

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model {

    use HasFactory;

    const ID = 'id';
    const TYPE_ID = 'type_id';    
    const BARCODE = 'barcode';
    const IS_VALID = 'is_valid';

    protected $guarded = [];

    protected function serializeDate(DateTimeInterface $date) {
        return $date->format('Y-m-d H:i:s');
    }

    function type() {
        return $this->belongsTo(TicketType::class);
    }

    function item() {
        return $this->hasOne(TicketSaleItem::class);
    }

    function scopeValidate($query, $state = true) {
        $query->where(Self::IS_VALID, $state);
    }
}
