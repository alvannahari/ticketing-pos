<?php

namespace App\Exports;

use App\Models\ProductSale;
use App\Models\ProductSaleItem;
use App\Models\TypePayment;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ReportStore implements FromArray, WithHeadings, WithCustomStartCell, WithMapping, ShouldAutoSize, WithStyles, WithColumnFormatting {

    private $count = 0;
    private $start, $end, $admin;
    private $qty, $total;

    function __construct($start, $end, $admin_id) {
        $this->start = $start;
        $this->end = $end;
        $this->admin = User::find($admin_id);
    }

    public function array() : array {
        $admin_id = $this->admin->id ?? 0;

        return ProductSaleItem::with('sale.payment')
            ->when($admin_id != 0, function ($query) use ($admin_id) {
                return $query->whereHas('sale', function($q) use ($admin_id) {
                    $q->where(ProductSale::USER_ID, $admin_id);
                });
            })
            ->whereDate(ProductSaleItem::CREATED_AT, '>=', $this->start)
            ->whereDate(ProductSaleItem::CREATED_AT, '<=', $this->end)
            ->orderBy(ProductSaleItem::CREATED_AT)->get()->toArray();
    }

    public function startCell(): string {
        return 'B6';
    }

    public function headings(): array {
        return [
            'No.',
            'ID Transaksi',
            'Tanggal',
            'Nama Barang',
            'Kode Barang',
            'Cara Bayar',
            'QTY',
            'Harga',
            'Jumlah'
        ];
    }

    public function map($athletes): array {
        $athlete = json_decode(json_encode($athletes),true);
        $this->qty += $athlete[ProductSaleItem::QTY];
        $this->total += $athlete[ProductSaleItem::QTY] * $athlete[ProductSaleItem::PRICE];

        return [
            ++$this->count,
            $athlete['sale'][ProductSale::ID],
            date("d/m/Y", strtotime($athlete[ProductSaleItem::CREATED_AT])),
            $athlete[ProductSaleItem::NAME],
            $athlete[ProductSaleItem::CODE],
            $athlete['sale']['payment'][TypePayment::NAME],
            $athlete[ProductSaleItem::QTY],
            $athlete[ProductSaleItem::PRICE],
            $athlete[ProductSaleItem::QTY] * $athlete[ProductSaleItem::PRICE]
        ];
    }

    public function columnFormats(): array {
        return [
            'I' => '#,##0',
            'J' => '#,##0',
        ];
    }

    function styles(Worksheet $sheet) {
        $sheet->getStyle('6')->getFont()->setBold(true);
        // $sheet->getStyle('6')->getFont()->setSize(12);
        $sheet->getStyle('6')->getAlignment()->setVertical('center');
        $sheet->getStyle('6')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('B6:J6')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('b3dafc');
        $sheet->getStyle('B6:J6')->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN)->setColor(new Color('808080'));
        // $sheet->getStyle('A1:F1')->getFont()->getColor()->setRGB('0000ff');
        $sheet->getDefaultRowDimension()->setRowHeight(18);
        $sheet->getStyle('B')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('C')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('D')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('F')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('G')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('H')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('I')->getAlignment()->setHorizontal('right');
        $sheet->getStyle('J')->getAlignment()->setHorizontal('right');

        $sheet->getColumnDimension('A')->setAutoSize(false);
        $sheet->getColumnDimension('A')->setWidth(2);

        $sheet->mergeCells('B2:J2')->setCellValue('B2', strtoupper(config('app.name')));
        // $sheet->getStyle('B2')->getFont()->setSize(14);
        $sheet->getStyle('B2')->getFont()->setBold(true);
        $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
        
        // $sheet->mergeCells('B3:K3')->setCellValue('B3', now()->toDateTimeString());
        $sheet->mergeCells('B3:J3')->setCellValue('B3', 'PACITAN');
        $sheet->getStyle('B3')->getFont()->setBold(true);
        $sheet->getStyle('B3')->getAlignment()->setHorizontal('center');

        $sheet->mergeCells('B4:I4')->setCellValue('B4', empty($this->admin) ? 'Laporan Penjualan Store - Seluruh Petugas' : 'Laporan Penjualan Store - '.$this->admin->name);
        $sheet->getStyle('B4')->getAlignment()->setHorizontal('center');

        $high_row = ($sheet->getHighestRow()+1);
        $sheet->mergeCells('B'.$high_row.':G'.$high_row)->setCellValue('B'.$high_row, strtoupper("TOTAL"));
        $sheet->getStyle('B'.$high_row.':J'.$high_row)->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('f2f2f2');
        $sheet->getStyle('B'.$high_row.':J'.$high_row)->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN)->setColor(new Color('808080'));
        $sheet->getStyle($high_row)->getFont()->setBold(true);
        $sheet->getStyle($high_row)->getAlignment()->setVertical('center');
        $sheet->getStyle('H'.$high_row)->getAlignment()->setHorizontal('center');
        $sheet->setCellValueExplicit('H'.$high_row, number_format($this->qty, 0, '', '.'), DataType::TYPE_STRING);
        $sheet->setCellValueExplicit('J'.$high_row, number_format($this->total, 0, '', '.'), DataType::TYPE_STRING);

        // another way to add total row
        // $sheet->setCellValue('H'.$high_row, '=SUM(H7:H' . ($high_row-1) . ')');
    }
}
