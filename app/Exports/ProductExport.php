<?php

namespace App\Exports;

use App\Models\Product;
use App\Models\TicketSale;
use App\Models\TicketSaleItem;
use App\Models\TypePayment;
use App\Models\User;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class ProductExport implements FromArray, WithHeadings, WithCustomStartCell, WithMapping, ShouldAutoSize, WithStyles, WithColumnFormatting  {

    private $count = 0;

    public function array() : array {
        return Product::get()->toArray();
    }

    public function startCell(): string {
        return 'B6';
    }

    public function headings(): array {
        return [
            'No.',
            'ID Produk',
            'Nama',
            'Kode',
            'Inventory',
            'Harga',
            'Deskripsi'
        ];
    }

    public function map($product): array {
        $drawing = new Drawing;
        return [
            ++$this->count,
            $product[Product::ID],
            $product[Product::NAME],
            $product[Product::CODE],
            $product[Product::INVENTORY],
            $product[Product::PRICE],
            $product[Product::DESC]
        ];
    }

    public function columnFormats(): array {
        return [
            'E' => '@',
            'G' => '#,##0'
        ];
    }

    function styles(Worksheet $sheet) {
        $sheet->getStyle('6')->getFont()->setBold(true);
        // $sheet->getStyle('6')->getFont()->setSize(12);
        $sheet->getStyle('6')->getAlignment()->setVertical('center');
        $sheet->getStyle('6')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('B6:H6')->getFill()->setFillType(Fill::FILL_SOLID)->getStartColor()->setARGB('b3dafc');
        $sheet->getStyle('B6:H6')->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN)->setColor(new Color('808080'));
        // $sheet->getStyle('A1:F1')->getFont()->getColor()->setRGB('0000ff');
        // $sheet->getDefaultRowDimension()->setRowHeight(18);
        // $sheet->getRowDimension(7)->setRowHeight(50);
        $sheet->getStyle('B')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('B')->getAlignment()->setVertical('center');
        $sheet->getStyle('C')->getAlignment()->setVertical('center');
        $sheet->getStyle('D')->getAlignment()->setVertical('center');
        $sheet->getStyle('E')->getAlignment()->setVertical('center');
        $sheet->getStyle('F')->getAlignment()->setHorizontal('right');
        $sheet->getStyle('F')->getAlignment()->setVertical('center');
        $sheet->getStyle('G')->getAlignment()->setHorizontal('right');
        $sheet->getStyle('G')->getAlignment()->setVertical('center');
        // $sheet->getStyle('H')->getAlignment()->setWrapText(true)->setVertical('center');

        $sheet->getColumnDimension('A')->setAutoSize(false);
        $sheet->getColumnDimension('A')->setWidth(2);

        $sheet->mergeCells('B2:H2')->setCellValue('B2', strtoupper(config('app.name')));
        // $sheet->getStyle('B2')->getFont()->setSize(14);
        $sheet->getStyle('B2')->getFont()->setBold(true);
        $sheet->getStyle('B2')->getAlignment()->setHorizontal('center');
        
        // $sheet->mergeCells('B3:K3')->setCellValue('B3', now()->toDateTimeString());
        $sheet->mergeCells('B3:H3')->setCellValue('B3', 'PACITAN');
        $sheet->getStyle('B3')->getFont()->setBold(true);
        $sheet->getStyle('B3')->getAlignment()->setHorizontal('center');

        $sheet->mergeCells('B4:H4')->setCellValue('B4', 'Katalog Produk');
        $sheet->getStyle('B4')->getAlignment()->setHorizontal('center');

        $sheet->getColumnDimension('H')->setWidth(40);
    }
}
