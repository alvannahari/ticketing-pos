<?php

namespace App\Http\Controllers;

use App\Imports\TicketImport;
use App\Models\Ticket;
use App\Models\TicketType;
use Illuminate\Http\Request;
use Excel;
use DataTables;
use Illuminate\Database\QueryException;

class TicketController extends Controller {

    function __construct() {
        $this->middleware('permission:view-tickets')->only('index');
        $this->middleware('permission:delete-tickets')->only('destroy');
        $this->middleware('permission:import-tickets')->only('import');
    }

    public function valid() {
        if (request()->ajax()) {
            $tickets = Ticket::with(['type' => function($q) {
                $q->select(TicketType::ID, TicketType::NAME, TicketType::PRICE);
            }])->where(Ticket::IS_VALID, 1);
            
            return Datatables::eloquent($tickets)
                ->addIndexColumn()
                ->addColumn('type_name', function (Ticket $ticket) {
                    return $ticket->type->name;
                })
                ->addColumn('price', function(Ticket $ticket) {
                    return 'Rp. '. number_format($ticket->type->price, 0, '', '.');
                })
                ->addColumn('status', function($row) {
                    return '<span class="badge badge-sm badge-success"> Valid </span>';
                })
                ->addColumn('action', function($row){
                    return '<button class="btn btn-sm btn-danger" onclick="deleteTicket('.$row->id.')"><i class="fa fa-trash mr-1"></i> Hapus</button>';
                })
                ->rawColumns(["type_name",'status','action'])
                ->blacklist(['DT_RowIndex','action'])
                ->toJson();
        }
        return view('ticket.valid');
    }

    public function invalid() {
        if (request()->ajax()) {
            $tickets = Ticket::with(['type' => function($q) {
                $q->select(TicketType::ID, TicketType::NAME, TicketType::PRICE);
            }])->where(Ticket::IS_VALID, 0);

            return Datatables::eloquent($tickets)
                ->addIndexColumn()
                ->addColumn('type_name', function (Ticket $ticket) {
                    return $ticket->type->name;
                })
                ->addColumn('price', function(Ticket $ticket) {
                    return 'Rp. '. number_format($ticket->type->price, 0, '', '.');
                })
                ->addColumn('status', function($row) {
                    return '<span class="badge badge-sm badge-dark"> Invalid </span>';
                })
                ->addColumn('action', function($row){
                    return '<button class="btn btn-sm btn-danger" onclick="deleteTicket('.$row->id.')"><i class="fa fa-trash mr-1"></i> Hapus</button>';
                })
                ->rawColumns(["type_name",'action'])
                ->escapeColumns('status')
                ->blacklist(['DT_RowIndex','action'])
                ->toJson();
        }
        return view('ticket.invalid');
    }

    public function destroy(Ticket $ticket) {
        $ticket->delete();

        return response(['status' => true, 'message' => 'Data Tiket berhasil terhapus']);
    }

    function import(Request $request) {
        try {
            Excel::import(new TicketImport, $request->file('file')->store('temp'));
            return redirect()->back()->with(['message'=> 'Data berhasil di import', 'status' => 'success']);
        } catch (QueryException $q) {
            $errorCode = $q->errorInfo[1];
            if($errorCode == '1062'){
                return redirect()->back()->with(['message'=> 'Data barcode terdapat duplikasi.', 'status' => 'error']);
            }
            return redirect()->back()->with(['message'=> $q->getMessage(), 'status' => 'error']);
        }
    }
}
