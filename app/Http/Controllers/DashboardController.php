<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductSale;
use App\Models\ProductSaleItem;
use App\Models\Ticket;
use App\Models\TicketSale;
use App\Models\TicketSaleItem;
use App\Models\TicketType;
use App\Models\TypePayment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Spatie\Permission\Models\Role;

class DashboardController extends Controller {
    
    function dashboard() {
        if (auth()->user()->hasRole('owner')) 
            return redirect()->route('dashboard.ticket');
        
        if (auth()->user()->hasRole('ticket'))
            return view('dashboard.ticket', ['dashboard' => $this->_getDataStatisticTicket()]);
        
        if (auth()->user()->hasRole('pos'))
            return view('dashboard.store', ['dashboard' => $this->_getDataStatisticStore()]);
        
    }

    function ticket() {
        // $ticket = Ticket::where(Ticket::TYPE_ID, TicketType::TICKET_FREE)->where(Ticket::IS_VALID, 1)->count();
        // return response($ticket);

        if (!auth()->user()->hasRole('owner')) 
            return redirect()->route('dashboard');

            // $type_ticket_today = TicketSaleItem::select(DB::raw('sum(qty) as qty'), DB::raw("ticket_type_id"))
            // ->whereDate(TicketSaleItem::CREATED_AT, Carbon::yesterday())->groupBy('ticket_type_id')->get()->toArray();
            //                 return response($type_ticket_today);
            //                 return response($type_ticket_today[array_search(3, array_column($type_ticket_today, 'ticket_type_id'))]);

        // return response($this->_getDataStatisticTicket());
        return view('dashboard.ticket', ['dashboard' => $this->_getDataStatisticTicket()]);
    }

    function store() {
        if (!auth()->user()->hasRole('owner')) 
            return redirect()->route('dashboard');
            
        // return response(ProductSaleItem::with('product')->select(ProductSaleItem::NAME, ProductSaleItem::CODE, ProductSaleItem::PRICE, ProductSaleItem::PRODUCT_ID, DB::raw('SUM(qty) as `amount`'))->groupBy(ProductSaleItem::NAME, ProductSaleItem::CODE, ProductSaleItem::PRICE, ProductSaleItem::PRODUCT_ID)->orderBy('amount', 'DESC')->limit(10)->get());
        return view('dashboard.store', ['dashboard' => $this->_getDataStatisticStore()]);
    }

    private function _getDataStatisticTicket() {
        // $date_last_tickets = Ticket::latest()->select(Ticket::CREATED_AT)->first()->created_at;
        // $data['today']['ticket_available'] = Ticket::whereDate(Ticket::CREATED_AT, '>=', $date_last_tickets)->count() - TicketSaleItem::whereDate(TicketSaleItem::CREATED_AT, '>=', $date_last_tickets)->sum(TicketSaleItem::QTY);
        $data['today']['ticket_available'] = TicketType::sum(TicketType::INVENTORY);
        $data['today']['ticket_sold'] = TicketSale::withSum('items', TicketSaleItem::QTY)->whereDate(TicketSale::CREATED_AT, Carbon::now())->get()->sum('items_sum_qty');
        $data['today']['ticket_scanned'] = Ticket::validate(false)->whereDate(Ticket::UPDATED_AT, Carbon::now())->count();
        $data['today']['income'] = (int) TicketSale::whereDate(TicketSale::CREATED_AT, Carbon::now())->sum(TicketSale::FINAL_AMOUNT);
        $data['today']['income_type'] = TypePayment::withSum(['ticketSales' => function($query) {
            $query->whereDate(Ticket::CREATED_AT, Carbon::now());
        }], TicketSale::FINAL_AMOUNT)->get();

        // $ticket_added_today = Ticket::whereDate(Ticket::CREATED_AT, Carbon::now())->count();
        $data['state']['ticket_available'] = Ticket::whereDate(Ticket::CREATED_AT, Carbon::now())->count() > 0 ? true : ($data['today']['ticket_sold'] > 0 ? false : true);
        $ticket_sold_yesterday = TicketSale::withSum('items', TicketSaleItem::QTY)->whereDate(TicketSale::CREATED_AT, Carbon::yesterday())->get()->sum('items_sum_qty');
        $data['state']['ticket_sold'] = $data['today']['ticket_sold'] >= $ticket_sold_yesterday ? true : false;
        $data['today']['ticket_sold_percent'] = $this->getPercentage($data['today']['ticket_sold'], $ticket_sold_yesterday);
        $ticket_scanned_yesterday = Ticket::validate(false)->whereDate(Ticket::UPDATED_AT, Carbon::yesterday())->count();
        $data['state']['ticket_scanned'] = $data['today']['ticket_scanned'] >= $ticket_scanned_yesterday ? true : false;
        $ticket_sale_today = TicketSale::whereDate(TicketSale::CREATED_AT, Carbon::now())->sum(TicketSale::FINAL_AMOUNT);
        $ticket_sale_yesterday = TicketSale::whereDate(TicketSale::CREATED_AT, Carbon::yesterday())->sum(TicketSale::FINAL_AMOUNT);
        $data['state']['income'] = $ticket_sale_today >= $ticket_sale_yesterday ? true : false;

        $weekly = TicketSaleItem::select(
            DB::raw('sum(qty) as qty'), 
            DB::raw("DATE_FORMAT(created_at,'%d') as day")
        )
        ->whereBetween(TicketSaleItem::CREATED_AT, [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->groupBy('day')
        ->orderBy(TicketSaleItem::CREATED_AT)->get()->toArray();
        $data['weekly']['graph'] = $this->graphWeekly($weekly, true);
        $type_ticket_today = TicketSaleItem::select(DB::raw('sum(qty) as qty'), DB::raw("ticket_type_id"))
                            ->whereDate(TicketSaleItem::CREATED_AT, Carbon::now())->groupBy('ticket_type_id')->get();
        foreach ($type_ticket_today as $key => $value) {
            if ($value['ticket_type_id'] == TicketType::TICKET_FREE) {
                $data['weekly']['today']['free_ticket'] = (int) $value['qty'];
                $data['weekly']['percent']['free_ticket'] = $this->_getPercentageTickets((int) $value['qty'], $value['ticket_type_id']);
            } else if ($value['ticket_type_id'] == TicketType::TICKET_DOMESTIC) {
                $data['weekly']['today']['domestic_ticket'] = (int) $value['qty'];
                $data['weekly']['percent']['domestic_ticket'] = $this->_getPercentageTickets((int) $value['qty'], $value['ticket_type_id']);
            } else if ($value['ticket_type_id'] == TicketType::TICKET_NATIONAL) {
                $data['weekly']['today']['national_ticket'] = (int) $value['qty'];
                $data['weekly']['percent']['national_ticket'] = $this->_getPercentageTickets((int) $value['qty'], $value['ticket_type_id']);
            } else if ($value['ticket_type_id'] == TicketType::TICKET_INTERNATIONAL) {
                $data['weekly']['today']['international_ticket'] = (int) $value['qty'];
                $data['weekly']['percent']['international_ticket'] = $this->_getPercentageTickets((int) $value['qty'], $value['ticket_type_id']);
            }
        }

        // $ticket_sales = TicketSaleItem::select(TicketSaleItem::TICKET_TYPE_ID, DB::raw("SUM(qty) as total_qty"))
        // ->groupBy(TicketSaleItem::TICKET_TYPE_ID)->get();
        // $type_tickets = TicketType::withCount('tickets')->get();
        $type_tickets = TicketType::get();
        foreach ($type_tickets as $type) {
            $data['type_ticket']['label'][] = $type->name;
            // $qty = $type->tickets_count;
            // foreach ($ticket_sales as $ticket_sale) {
            //     if ($type->id === (int) $ticket_sale->ticket_type_id) {
            //         $qty = $type->tickets_count - $ticket_sale->total_qty;
            //         break;
            //     }
            // }
            $data['type_ticket']['inventory'][] = (int) $type->inventory;
        }

        $ticket_sales = TicketSaleItem::select(
            DB::raw('sum(qty) as qty'), 
            DB::raw('ticket_type_id'), 
            DB::raw("DATE_FORMAT(created_at,'%m') as month")
        )->whereYear(TicketSaleItem::CREATED_AT, Carbon::now()->format('Y'))->groupBy('month','ticket_type_id')->get()->toArray();
        $data['ticket_sales'] = $this->graphMonthly($ticket_sales);
        
        return $data;
    }
    
    private function _getDataStatisticStore() {
        $data['today']['sales'] = ProductSale::whereDate(ProductSale::CREATED_AT, Carbon::now())->count();
        $data['today']['product_sales'] = ProductSaleItem::whereDate(ProductSaleItem::CREATED_AT, Carbon::now())->sum(ProductSaleItem::QTY);
        $data['today']['products'] = Product::count();
        $data['today']['income'] = (int) ProductSale::whereDate(ProductSale::CREATED_AT, Carbon::now())->sum(ProductSale::FINAL_AMOUNT);
        $data['today']['income_type'] = TypePayment::withSum(['productSales' => function($query) {
            $query->whereDate(Ticket::CREATED_AT, Carbon::now());
        }], ProductSale::FINAL_AMOUNT)->get();

        $weekly = ProductSale::select(
            DB::raw('count(id) as qty'), 
            DB::raw("DATE_FORMAT(created_at,'%d') as day")
            )
            ->whereBetween(ProductSale::CREATED_AT, [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->groupBy('day')
        ->orderBy(ProductSale::CREATED_AT)->get()->toArray();
        $data['weekly'] = $this->graphWeekly($weekly);
        
        $monthly = ProductSale::select(
            DB::raw('count(id) as qty'),
            DB::raw("DATE_FORMAT(created_at,'%M') as month")
            )
            ->whereYear(ProductSale::CREATED_AT, Carbon::now()->year)->groupBy('month')
            ->orderBy(ProductSale::CREATED_AT)->get()->toArray();
        $data['monthly']['qty'] = array_column($monthly, 'qty');
        $data['monthly']['month'] = array_column($monthly, 'month');
        
        $data['bestsellers'] = ProductSaleItem::with('product')->select(
            ProductSaleItem::NAME, ProductSaleItem::CODE, ProductSaleItem::PRICE, ProductSaleItem::PRODUCT_ID, DB::raw('SUM(qty) as `amount`'))
            ->groupBy(ProductSaleItem::NAME, ProductSaleItem::CODE, ProductSaleItem::PRICE, ProductSaleItem::PRODUCT_ID)->orderBy('amount', 'DESC')
            ->limit(10)->get();

        $data['latest'] = ProductSaleItem::orderBy(ProductSaleItem::CREATED_AT, 'DESC')->limit(10)->get();

        return $data;
    }

    private function _getPercentageTickets(int $qty, $type_id) {
        $tickets_yesterday = TicketSaleItem::where(TicketSaleItem::TICKET_TYPE_ID, $type_id)
                                ->whereDate(TicketSaleItem::CREATED_AT, Carbon::yesterday())
                                ->get()->sum(TicketSaleItem::QTY);

        $percent = $this->getPercentage($qty, (int) $tickets_yesterday);

        return $percent;
    }
    
    function graphWeekly(array $activity, $date = false) {
        $data['label'] = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu','Minggu'];

        for ($i=0; $i<7; $i++){
            $data['qty'][$i] = 0;
            $thisDate = Carbon::now()->startOfWeek()->addDay($i)->format('d');
            foreach($activity as $key => $value) {
                if($value['day'] == $thisDate){
                    $data['qty'][$i] = (int) $value['qty'];
                    break;
                };
            };
            if ($date)
                $data['label'][$i] = $data['label'][$i].', '.Carbon::now()->startOfWeek()->addDays($i)->format('d M Y');
        };

        return $data;
    }

    function graphMonthly(array $activity) {
        $data['label'] = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        $visitor = [
            'free_ticket' => ['name' => 'Tiket Gratis'],
            'domestic_ticket' => ['name' => 'Tiket Domestik'],
            'national_ticket' => ['name' => 'Tiket Nasional'],
            'international_ticket' => ['name' => 'Tiket Internasional'],
        ];

        for ($i=0; $i<12; $i++){
            $visitor['free_ticket']['data'][$i] = 0;
            $visitor['domestic_ticket']['data'][$i] = 0;
            $visitor['national_ticket']['data'][$i] = 0;
            $visitor['international_ticket']['data'][$i] = 0;
            $thisMonth = Carbon::now()->startOfYear()->addMonths($i)->format('m');
            foreach($activity as $key => $value) {
                if($value['month'] == $thisMonth){
                    if ($value['ticket_type_id'] == TicketType::TICKET_FREE) $visitor['free_ticket']['data'][$i] = (int) $value['qty'];
                    else if ($value['ticket_type_id'] == TicketType::TICKET_DOMESTIC) $visitor['domestic_ticket']['data'][$i] = (int) $value['qty'];
                    else if ($value['ticket_type_id'] == TicketType::TICKET_NATIONAL) $visitor['national_ticket']['data'][$i] = (int) $value['qty'];
                    else if ($value['ticket_type_id'] == TicketType::TICKET_INTERNATIONAL) $visitor['international_ticket']['data'][$i] = (int) $value['qty'];
                }
            };
        };

        $data['visitor'] = $visitor;
        return $data;
    }
    
    function getPercentage(int $recent, int $previous): int {
        $minVal = min($recent, $previous);

        if (!$minVal) return 0;
        
        return (($recent - $previous) / $minVal) * 100;
    }
    
}
