<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductSale;
use App\Models\ProductSaleItem;
use App\Models\TypePayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller {

    function __construct() {
        $this->middleware('permission:create-sale-products')->only('index','store');
        $this->middleware('permission:view-sale-products')->only('show');
    }

    // function transaction_v1() {
    //     if (request()->ajax()) {
    //         $products = Product::get();
    //         return response(['status' => true, 'data' => $products]);
    //     }

    //     $payments = TypePayment::get();
    //     return view('product-sale.create', compact('payments'));
    // }

    function index() {
        if (request()->ajax()) {
            $searh = request()->input('search');
            $products = Product::when($searh, function($q, $searh) {
                return $q->where(Product::NAME, 'like', '%'.$searh.'%')->orWhere(Product::CODE, 'like', '%'.$searh.'%');
            })->get();

            return response(['status' => true, 'data' => $products]);
        }

        $this->customer_display('home');
        $products = Product::get();
        $payments = TypePayment::get();
        return view('product-sale.createv3', compact('payments','products'));
    }

    function store(Request $request) {
        $validator = Validator::make($request->all(), [
            ProductSale::TOTAL_PRICE => 'required|numeric',
            ProductSale::DISCOUNT => 'nullable|numeric',
            ProductSale::FINAL_AMOUNT => 'required|numeric',
            ProductSale::PAID_AMOUNT => 'required|numeric',
            ProductSale::CHANGE => 'required|numeric',
            'items' => 'required|array|min:1'
        ]);

        if ($validator->fails()) 
            return response(['status' => false, 'message' => 'Silahkan Periksa Form', 'errors' => $validator->errors()]);

        DB::beginTransaction();

        try {
            if ($request->input(ProductSale::PAID_AMOUNT) < $request->input(ProductSale::FINAL_AMOUNT))
                return response(['status' => false, 'message' => 'Silahkan Periksa Form', 'errors' => [ ProductSale::PAID_AMOUNT => 'Uang pembayaran tidak mencukupi.']]);

            $payload = $request->except('items');
            $payload[ProductSale::USER_ID] = auth()->id();
            $payload[ProductSale::CUST_NAME] = $request->input(ProductSale::CUST_NAME) ?? '-';
            $sale = ProductSale::create($payload);

            foreach ($request->input('items') as $key => $value) {
                $product = Product::find($value['id']);

                if ($product->inventory < $value['qty']) {
                    DB::rollBack();
                    return response(['status' => false, 'message' => 'Beberapa produk melebihi stok barang.', 'errors' => []]);
                }

                $product->decrementStok($value['qty']);

                $payload_item[ProductSaleItem::SALE_ID] = $sale->id;
                $payload_item[ProductSaleItem::PRODUCT_ID] = $product->id;
                $payload_item[ProductSaleItem::NAME] = $product->name;
                $payload_item[ProductSaleItem::DESC] = $product->desc;
                $payload_item[ProductSaleItem::CODE] = $product->code;
                $payload_item[ProductSaleItem::QTY] = $value['qty'];
                $payload_item[ProductSaleItem::CAPITAL_PRICE] = $product->capital_price;
                $payload_item[ProductSaleItem::PRICE] = $product->price;
                $payload_item[ProductSaleItem::PROFIT] = $value['qty'] * ($product->price - $product->capital_price);
                ProductSaleItem::create($payload_item);
            }

            $data['data']['transaction_id'] = $sale->id;
            $this->save_activity('Penjualan Produk', $data);

            DB::commit();
            return response(['status' => true, 'message' => 'baru berhasil tersimpan', 'data' => $sale]);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response(['status' => false, 'message' => $th->getMessage(), "errors" => []]);
        }
    }

    function show($transaction_id) {
        $transaction = ProductSale::with('items')->find($transaction_id);

        if (request()->ajax()) {
            if (empty($transaction)) 
                return response(['status' => false]);
            
            return response(['status' => true]);
        }

        return view('product-sale.print', compact('transaction'));
    }
}
