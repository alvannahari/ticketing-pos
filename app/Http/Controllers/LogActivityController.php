<?php

namespace App\Http\Controllers;

use App\Models\LogActivity;
use App\Models\ProductSale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class LogActivityController extends Controller {

    function __construct() {
        $this->middleware('permission:view-activities')->only('index');
    }

    public function index() {
        // return response(auth()->user()->roles);
        if (request()->ajax()) {
            $activities = LogActivity::with('user')->orderBy(LogActivity::CREATED_AT, 'DESC')->get();
            return response(['status' => true, 'data' => $activities]);
        }
        return view('log-activity.index');
    }

    public function destroy(LogActivity $logActivity) {
        $logActivity->delete();

        return response(['status' => true, 'message' => 'Aktifitas Petugas Berhasil Terhapus']);
    }

    function synchronized_data() {
        DB::beginTransaction();
        $log_name = 'Sinkronisasi Data Server';

        try {
            $latest = LogActivity::where(LogActivity::LOG_NAME, $log_name)->latest()->first();

            if ($latest == null) {
                $transaction_products = ProductSale::with('items')->get()->toArray();
            } else {
                $transaction_products = ProductSale::with('items')->whereDate(ProductSale::CREATED_AT, '>=', $latest->created_at)->get()->toArray();
            }

            if (count($transaction_products) == 0) {
                DB::commit();
                return response(['status' => true, 'message' => 'Data Transaksi Berhasil di sinkronisasikan.']);
            }

            $response = Http::post(config('app.url_hosting')."/api/synchronized-data", [
            // $response = Http::post("http://127.0.0.1:8000/api/synchronized-data", [
                'transaction_products' => $transaction_products,
            ]);
            // return response(['status' => true, 'message' => $response]);

            // $response = Http::get("https://google.com");

            if (!$response->ok()) {
                DB::rollBack();
                return response(['status' => false, 'message' => $response->json()]);
            }
            
            $this->save_activity($log_name, null);
            DB::commit();
            return response(['status' => true, 'message' => 'Data Transaksi Berhasil di sinkronisasikan.']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response(['status' => false, 'message' => $th->getMessage()]);
        }
    }
}
