<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends Controller {

    function __construct() {
        $this->middleware('permission:view-users')->only('index');
        $this->middleware('permission:create-users')->only('store');
        $this->middleware('permission:edit-users')->only('changeRole','changePassword');
        $this->middleware('permission:delete-users')->only('destroy');
    }
    
    function index() {
        if (request()->ajax()) {
            // $users = User::with('roles')->where(User::ROLE_ID, "<>", User::ROLE_OWNER)->get();
            $users = User::with('roles')->where(User::ID, '<>', 1)->get();
            return response(['status' => true, 'data' => $users]);
        }

        $roles = Role::get();
        return view('user.index', compact('roles'));
    }

    function store(Request $request) {
        $validator = Validator::make($request->all(), [
            User::NAME => 'required|string',
            User::USERNAME => 'required|alpha_dash',
            User::ROLE_ID => 'required|numeric',
            User::PASSWORD => 'required|min:4|confirmed'
        ]);

        if ($validator->fails()) 
            return response(['status' => false, 'errors' => $validator->errors()]);

        DB::beginTransaction();
        try {
            $payload = $validator->validate();
            $payload[User::PASSWORD] = bcrypt($request->input([User::PASSWORD]));
            $user = User::create($payload);
    
            $roles = Role::get();
            foreach ($roles as $role) {
                if ($role->id == $user->role_id) $user->assignRole($role->name);
            }

            DB::commit();
            return response(['status' => true, 'message' => 'Data akun petugas berhasil dibuat.']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return response(['status' => false, 'message' => $th->getMessage()], 500);
        }
    }

    function changeRole(Request $request, User $user) {
        $roles = Role::get();
        foreach ($roles as $role) {
            if ($role->id == $request->input(User::ROLE_ID)) 
                $user->syncRoles($role->name);
        }
        $user->update([
            User::ROLE_ID => $request->input(User::ROLE_ID)
        ]);
        return response(['status' => true, 'message' => 'Role petugas berhasil diperbarui.']);
    }
    
    function changePassword(Request $request, User $user) {
        $validator = Validator::make($request->all(), [
            'new_password' => 'required|confirmed'
        ]);

        if ($validator->fails()) 
            return response(['status' => false, 'errors' => $validator->errors()]);

        $user->update([
            User::PASSWORD => bcrypt($request->input('new_password'))
        ]);
    
        return response(['status' => true, 'message' => 'Password akun berhasil diperbarui.']);
    }

    function destroy(User $user) {
        $user->delete();

        return response(['status' => true, 'message' => 'Data pengguna berhasil terhapus.']);
    }
}
