<?php

namespace App\Http\Controllers;

use App\Models\ProductSale;
use Illuminate\Http\Request;
use DataTables;

class ProductSaleController extends Controller {

    function __construct() {
        $this->middleware('permission:view-sale-products')->only('index', 'show');
        $this->middleware('permission:delete-sale-products')->only('destroy');
    }
    
    public function index() {
        if (request()->ajax()) {
            // $sales = ProductSale::with('user','payment','items')->latest()->get();
            // return response(['status' => true, 'data' => $sales]);
            
            $sales = ProductSale::with('user','payment','items');
            return Datatables::eloquent($sales)
                ->addIndexColumn()
                ->addColumn('name', function (ProductSale $sale) {
                    return '<p class="mb-0">'.$sale->cust_name.'</p><small>'.$sale->user->name.'</small>';
                })
                ->addColumn('total', function(ProductSale $sale) {
                    return '<p class="mb-1">Rp. '.number_format($sale->total_price, 0, '', '.').'</p><small>- Rp. '.number_format($sale->discount, 0, '', '.').'</small>';
                })
                ->addColumn('final', function(ProductSale $sale) {
                    return '<h6 class="mb-0"><b>Rp. '.number_format($sale->final_amount, 0, '', '.').'</b></h6><small>'.$sale->id.'</small>';
                })
                ->addColumn('paid', function(ProductSale $sale) {
                    return '<p class="mb-0">Rp. '.number_format($sale->paid_amount, 0, '', '.').'</p><small> Rp. '.number_format($sale->change, 0, '', '.').'</small>';
                })
                ->addColumn('products', function($row){
                    $html = '<small>';
                    foreach ($row->items as $key => $value) {
                        $html .= $value->qty.' x '.$value->name.' @ '.$value->price.'<br>';
                    };
                    return $html .= '</small>';
                })
                ->addColumn('status', function(ProductSale $sale) {
                    if ($sale->is_completed)
                        return '<span class="badge badge-sm badge-success"> Selesai </span>';
                    else 
                        return '<span class="badge badge-sm badge-secondary"> Pending </span>';
                })
                ->addColumn('action', function($row){
                    return '<a href="'.route('product-sales.show',$row->id).'" class="btn btn-sm btn-info text-white"><i class="fa fa-pen mr-1"></i> Detail</button>';
                })
                ->rawColumns(["name", 'total', 'final', 'paid', 'products', 'status', 'action'])
                ->blacklist(['DT_RowIndex','action'])
                ->toJson();
        }
        return view('product-sale.index');
    }

    public function show(ProductSale $productSale) {
        $sale = $productSale->load('user','payment','items');
        return view('product-sale.show', compact('sale'));
    }

    public function destroy(ProductSale $productSale) {
        $productSale->delete();

        return response(['status' => true, 'message' => 'Data penjualan produk berhasil terhapus.']);
    }
}
