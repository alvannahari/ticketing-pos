<?php

namespace App\Http\Controllers;

use App\Exports\ReportStore;
use App\Exports\ReportTickets;
use App\Models\ProductSaleItem;
use App\Models\Ticket;
use App\Models\TicketSale;
use App\Models\TicketSaleItem;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Excel;

class ReportController extends Controller {
    
    function __construct() {
        $this->middleware('permission:view-users');
    }

    function reportTickets() {
        if (request()->ajax()) {
            $start = request()->input('start');
            $end = request()->input('end');
            $admin_id = request()->input('admin_id');

            $data = TicketSaleItem::with('sale.payment')
                    ->when($admin_id != "0", function ($query) use ($admin_id) {
                        return $query->whereHas('sale', function($q) use ($admin_id) {
                            $q->where(TicketSale::USER_ID, $admin_id);
                        });
                    })
                    ->whereDate(TicketSaleItem::CREATED_AT, '>=', $start)
                    ->whereDate(TicketSaleItem::CREATED_AT, '<=', $end)
                    ->orderBy(TicketSaleItem::CREATED_AT)->get();

            return response(['status' => true, 'data' => $data, 'input' => request()->all()]);
        }

        $admins = User::where(User::ROLE_ID, User::ROLE_TICKET)->get();
        return view('report.ticket', compact('admins'));
    }

    function exportTickets(Request $request) {
        $start = $request->input('start');
        $end = $request->input('end');
        $admin_id = $request->input('admin_id');

        return Excel::download(new ReportTickets($start, $end, $admin_id), 'Laporan Tiket MUSEUM SBYANI - '.Carbon::now()->format('d-m-Y').'.xlsx');
    }

    function reportStore() {
        if (request()->ajax()) {
            $start = request()->input('start');
            $end = request()->input('end');
            $admin_id = request()->input('admin_id');

            $data = ProductSaleItem::with('sale.payment')
                    ->when($admin_id != "0", function ($query) use ($admin_id) {
                        return $query->whereHas('sale', function($q) use ($admin_id) {
                            $q->where(TicketSale::USER_ID, $admin_id);
                        });
                    })
                    ->whereDate(ProductSaleItem::CREATED_AT, '>=', $start)
                    ->whereDate(ProductSaleItem::CREATED_AT, '<=', $end)
                    ->orderBy(ProductSaleItem::CREATED_AT)->get();

            return response(['status' => true, 'data' => $data, 'input' => request()->all()]);
        }

        $admins = User::where(User::ROLE_ID, User::ROLE_POS)->get();
        return view('report.store', compact('admins'));
    }

    function exportStore(Request $request) {
        $start = $request->input('start');
        $end = $request->input('end');
        $admin_id = $request->input('admin_id');

        return Excel::download(new ReportStore($start, $end, $admin_id), 'Laporan Penjualan Store MUSEUM SBYANI - '.Carbon::now()->format('d-m-Y').'I.xlsx');
    }
}
