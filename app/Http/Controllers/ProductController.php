<?php

namespace App\Http\Controllers;

use App\Exports\ProductExport;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Excel;

class ProductController extends Controller {
    
    function __construct() {
        $this->middleware('permission:view-products')->only('index');
        $this->middleware('permission:create-products')->only('create','store');
        $this->middleware('permission:edit-products')->only('show','update');
        $this->middleware('permission:delete-products')->only('destroy');
    }

    public function index() {
        if (request()->ajax()) {
            $products = Product::get();
            return response(['status' => true, 'data' => $products]);
        }

        return view('product.index');
    }

    public function create() {
        return view('product.create');
    }

    public function store(Request $request) {
        $request->validate([
            Product::NAME => 'required|string',
            Product::CODE => 'required|min:6|unique:products,code',
            Product::INVENTORY => 'required|numeric',
            Product::CAPITAL_PRICE => 'numeric',
            Product::PRICE => 'required|numeric|min:1',
            Product::IMAGE => 'image|mimes:jpg,jpeg,png|max:2048',
            Product::DESC => 'nullable',
        ]);
        
        DB::beginTransaction();
        
        try {
            $product_id = Product::generateProductId();
            $payload = $request->except(Product::IMAGE);
            $payload[Product::ID] = $product_id;
            $capital_price = $request->input(Product::CAPITAL_PRICE) != '' ? $request->input(Product::CAPITAL_PRICE) : 0;
            $payload[Product::CAPITAL_PRICE] = $capital_price != 0 ? $capital_price : $request->input(Product::PRICE) - 1000;
            
            if ($request->has(Product::IMAGE)) {
                $image = Storage::disk('public')->put(Product::IMAGE_PATH, $request->file(Product::IMAGE));
                $payload[Product::IMAGE] = basename($image);
            }
    
            Product::create($payload);
    
            DB::commit();
            return redirect()->route('products.index')->with(['status' => 'Produk', 'message' => 'baru berhasil tersimpan.']);
        } catch (\Throwable $th) {
            DB::rollBack();
            return redirect()->back()->with(['status' => 'error', 'message' => 'Produk gagal tersimpan.']);
        }
    }

    public function show(Product $product) {
        // return response($product);
        return view('product.show', compact('product'));
    }

    public function update(Request $request, Product $product) {
        $request->validate([
            Product::NAME => 'required|string',
            Product::CODE => 'required|min:6|unique:products,code,'.$product->id,
            Product::INVENTORY => 'required|numeric',
            Product::CAPITAL_PRICE => 'numeric',
            Product::PRICE => 'required|numeric|min:1',
            Product::IMAGE => 'image|mimes:jpg,jpeg,png|max:2048',
            Product::DESC => 'nullable',
        ]);

        if (auth()->user()->hasRole('owner'))
            $payload = $request->except(Product::IMAGE);
        else
            $payload = $request->except(Product::IMAGE, Product::INVENTORY);
        
        $capital_price = $request->input(Product::CAPITAL_PRICE) != '' ? $request->input(Product::CAPITAL_PRICE) : 0;
        $payload[Product::CAPITAL_PRICE] = $capital_price != 0 ? $capital_price : $request->input(Product::PRICE) - 1000;

        if ($request->has(Product::IMAGE)) {
            $arr_image = explode('/',$product->image);
            if (end($arr_image) != 'not_found.png') {
                Storage::disk('public')->delete(Product::IMAGE_PATH.end($arr_image));
            }
            $image = Storage::disk('public')->put(Product::IMAGE_PATH, $request->file(Product::IMAGE));
            $payload[Product::IMAGE] = basename($image);
        }

        $product->update($payload);

        return redirect()->route('products.index')->with(['status' => 'Produk', 'message' => 'berhasil diperbarui.']);
    }

    public function destroy(Product $product) {
        $arr_image = explode('/',$product->image);
        if (end($arr_image) != 'not_found.png') {
            Storage::disk('public')->delete(Product::IMAGE_PATH.end($arr_image));
        }
        $product->delete();

        return response(['status' => true, 'message' => 'Produk Berhasil Terhapus.']);
    }

    function print_barcode($code) {
        $product = Product::where(Product::CODE, $code)->firstOrFail();

        return view('product.barcode', compact('product'));
    }

    function display_product($name, $price) {
        try {
            $this->customer_display('product', $name, (int) $price);

            return response(['status' => true, 'message' => 'Display changed.']);
        } catch (\Throwable $th) {
            return response(['status' => false, 'message' => $th->getMessage()]);
        }
    }

    function export() {
        return Excel::download(new ProductExport(), 'Katalog Produk MUSEUM GALERI SBYANI - '.Carbon::now()->format('d-m-Y').'.xlsx');
    }
}
