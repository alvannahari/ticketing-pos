<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\ProductSale;
use App\Models\ProductSaleItem;
use Illuminate\Http\Request;

class TransactionController extends Controller {
    
    function synchronized_data(Request $request) {
        $transaction_products = $request->input('transaction_products');

        foreach ($transaction_products as $transaction) {
            ProductSale::create([
                ProductSale::ID => $transaction->id,
                ProductSale::USER_ID => $transaction->user_id,
                ProductSale::PAYMENT_ID => $transaction->payment_id,
                ProductSale::CUST_NAME => $transaction->cust_name,
                ProductSale::TOTAL_PRICE => $transaction->total_price,
                ProductSale::DISCOUNT => $transaction->discount,
                ProductSale::FINAL_AMOUNT => $transaction->final_amount,
                ProductSale::PAID_AMOUNT => $transaction->paid_amount,
                ProductSale::NOTE => $transaction->note,
            ]);

            foreach ($transaction['items'] as $item) {
                ProductSaleItem::create([
                    ProductSaleItem::ID => $item->id,
                    ProductSaleItem::SALE_ID => $item->sale_id,
                    ProductSaleItem::PRODUCT_ID => $item->product_id,
                    ProductSaleItem::NAME => $item->name,
                    ProductSaleItem::QTY => $item->qty,
                    ProductSaleItem::CODE => $item->code,
                    ProductSaleItem::PRICE => $item->price,
                    ProductSaleItem::CAPITAL_PRICE => $item->capital_price,
                    ProductSaleItem::PROFIT => $item->profit,
                    ProductSaleItem::DESC => $item->note,
                ]);
            }
        }

        return $this->response_success('Data berhasil disinkronkan.', null);
    }
}
