<?php

namespace App\Http\Controllers\Api\V1\Ticket;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleActivityRequest;
use App\Models\Ticket;
use App\Models\TicketSale;
use App\Models\TicketSaleItem;
use App\Models\TicketType;
use App\Models\TypePayment;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use charlieuki\ReceiptPrinter\ReceiptPrinter as ReceiptPrinter;
use Illuminate\Http\Request;

class TicketController extends Controller {
    
    function get_type_ticket() {
        $tickets = TicketType::get();

        return $this->response_success('Data seluruh tiket berhasil didapatkan.', $tickets);
    }

    function get_type_payment() {
        $types = TypePayment::get();

        return $this->response_success('Data jenis pembayaran berhasil didapatkan.', $types);
    }

    function store_transaction(RoleActivityRequest $request) {
        $validator = Validator::make($request->all(), [
            TicketSale::PAYMENT_ID => 'required|numeric',
            TicketSale::CUST_NAME => 'nullable',
            TicketSale::DISCOUNT => 'nullable|numeric',
            TicketSale::FINAL_AMOUNT => 'required|numeric',
            TicketSale::TOTAL_PRICE => 'required|numeric',
            TicketSale::NOTE => 'nullable',
            'tickets' => 'required|array|min:1',
        ]);

        if ($validator->fails()) 
            return $this->response_error('Terdapat Kesalahan', $validator->errors(), 400);

        DB::beginTransaction();

        try {
            $payload = $request->except('tickets', 'device');
            $payload[TicketSale::USER_ID] = auth()->id();
            $payload[TicketSale::DISCOUNT] = $payload[TicketSale::DISCOUNT] / 100 * $payload[TicketSale::TOTAL_PRICE];
            $sale = TicketSale::create($payload);

            $tickets = $request->input('tickets');
            $total_price = 0;
    
            foreach ($tickets as $key => $value) {
                // $tickets = Ticket::where(Ticket::TYPE_ID, $value['type_id'])->validate()->count();

                // if ($tickets < $value['qty']) {
                //     DB::rollBack();
                //     return $this->response_error('Jumlah tiket telah habis.', null, 400);
                // }

                // $total_price += $tickets->price * $value['qty'];

                $type = TicketType::find($value['type_id']);
                TicketSaleItem::create([
                    TicketSaleItem::SALE_ID => $sale->id,
                    TicketSaleItem::TICKET_TYPE_ID => $value['type_id'],
                    TicketSaleItem::NAME => $type->name,
                    TicketSaleItem::PRICE => $type->price,
                    TicketSaleItem::QTY => $value['qty'],
                ]);

                if (TicketType::find($value['type_id'])->inventory >= $value['qty']) 
                    TicketType::find($value['type_id'])->decrementInventory($value['qty']);
            }
    
            // if ($total_price != $request->input(TicketSale::TOTAL_PRICE)) {
            //     DB::rollBack();
            //     return $this->response_error('Total harga mengalami perubahan.', null, 400);
            // }

            $activity['device'] = $request->input('device');
            $activity['role'] = $request->input('role');
            $activity['data'] = array_merge(['transaction_id' => $sale->id],$request->except('device','role','tickets'));
            $this->save_activity('Penjualan tiket', $activity);

            DB::commit();
            return $this->response_success('Data transaksi berhasil di proses.', $sale);
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->response_error($th->getMessage(), null, 403);
        }
    }

    function list_transaction_history(Request $request) {
        $histories = TicketSale::withSum('items', TicketSaleItem::QTY)
                    ->when($request->input('start_date'), function ($query) use ($request) {
                        $query->whereDate(TicketSaleItem::CREATED_AT, '>=', $request->input('start_date'));
                    })
                    ->when($request->input('end_date'), function ($query) use ($request) {
                        $query->whereDate(TicketSaleItem::CREATED_AT, '<=', $request->input('end_date'));
                    })
                    ->where(TicketSale::USER_ID, $request->user()->id)->get();

        return $this->response_success('Data penjualan tiket.', $histories);
    }

    function detail_transaction(Request $request) {
        $validator = Validator::make($request->all(), [
            TicketSale::ID => 'required'
        ]);

        if ($validator->fails())
            return $this->response_error('Terdapat Kesalahan Formulir Tiket', $validator->errors(), 400);

        $sale = TicketSale::with('items')->find($request->input(TicketSale::ID));

        if ($sale == null)
            return $this->response_error('Data penjualan tiket tidak ditemukan.',null, 404);

        return $this->response_success('Data penjualan tiket.', $sale);
    }

    function scan_barcode(Request $request) { 
        $validator = Validator::make($request->all(), [
            Ticket::BARCODE => 'required'
        ]);

        if ($validator->fails())
            return $this->response_error('Terdapat Kesalahan', $validator->errors(), 400);

        $ticket = Ticket::where(Ticket::BARCODE, $request->input(Ticket::BARCODE))->first();

        if ($ticket == null)
            return $this->response_error('Data Barcode Tidak Ditemukan.', null, 404);

        if (!$ticket->is_valid) 
            return $this->response_error('Tiket sudah digunakan atau telah dipindai.', null, 401);

        $ticket->update([ Ticket::IS_VALID => false ]);

        return $this->response_success('Tiket berhasil dipindai.', null);
    }

    function list_entrance_history(Request $request) { 
        $histories = Ticket::validate(false)
                    ->when($request->input('start_date'), function ($query) use ($request) {
                        $query->whereDate(TicketSaleItem::UPDATED_AT, '>=', $request->input('start_date'));
                    })
                    ->when($request->input('end_date'), function ($query) use ($request) {
                        $query->whereDate(TicketSaleItem::UPDATED_AT, '<=', $request->input('end_date'));
                    })
                    // ->whereDate(TicketSaleItem::UPDATED_AT, '>=', $request->input('start_date'))
                    // ->whereDate(TicketSaleItem::UPDATED_AT, '<=', $request->input('end_date'))
                    ->latest(Ticket::UPDATED_AT)->get();

        return $this->response_success('Data penjualan tiket.', $histories);
    }
}
