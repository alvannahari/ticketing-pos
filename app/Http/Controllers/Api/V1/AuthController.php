<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleActivityRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller {
    
    function login(Request $request) {
        $validator = Validator::make($request->all(), [
            User::USERNAME      => ['required','alpha_dash'],
            User::PASSWORD   => ['required','string']
        ]);

        if ($validator->fails()) 
            return $this->response_error('Terjadi kesalahan permintaan', $validator->errors(), 400);

        if (!Auth::attempt($validator->validate())) 
            return $this->response_error('Username atau Password Tidak Cocok!', null, 401);
        
        $user = User::where(User::USERNAME, $request->input(User::USERNAME))->firstOrFail();

        // if ($user->getRoleNames() == 'owner') return $this->response_error('Anda tidak memiliki akses!', null, 403);

        $user->tokens()->delete();

        $response['user'] = $user;
        $response['role'] = $user->getRoleNames();
        $response['access_token'] = $user->createToken('auth_token')->plainTextToken;
        $response['token_type'] = 'Bearer';

        unset($response['user']['roles']);

        return $this->response_success('Login Berhasil.', $response);
    }

    function role_activity(RoleActivityRequest $request) {
        $data['device'] = $request->input('device');
        $data['role'] = $request->input('role');
        $data['data'] = $request->except('device','role','desc');
        $this->save_activity('Login Role Activity', $data, $request->input('desc'));

        return $this->response_success('Aktifitas login role berhasil tersimpan.', null);
    }

    function logout() {
        $user = request()->user();
        $user->tokens()->delete();

        return $this->response_success('Akun berhasil dikeluarkan.', null);
    }
}
