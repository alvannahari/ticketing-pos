<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    function login(Request $request) {
        $this->validator($request);

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // $this->incrementLoginAttempts($request);
        return redirect()->back()->with('message', 'Username Atau Password Tidak Cocok !!');
    }

    protected function validator(Request $request) {
        $validator = Validator::make($request->all(), [
            User::USERNAME      => ['required','alpha_dash'],
            User::PASSWORD   => ['required','string'],
        ], [
            'alpha_dash' => ':attribute tidak boleh mengandung spasi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        };
    }

    function username() {
        return User::USERNAME;
    }

    protected function sendLoginResponse(Request $request) {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        $this->customer_display('home');

        return redirect()->intended($this->redirectPath());
    }

    public function logout(Request $request) {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('dashboard');
    }
}
