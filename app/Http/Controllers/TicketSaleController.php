<?php

namespace App\Http\Controllers;

use App\Models\TicketSale;
use App\Models\TicketSaleItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;

class TicketSaleController extends Controller {

    function __construct() {
        $this->middleware('permission:view-sale-tickets')->only('index');
    }

    public function index() {
        if (request()->ajax()) {
            // $sales = TicketSale::with('user','payment','items')->withSum('items', TicketSaleItem::QTY)->latest()->get();
            // return response(['status' => true, 'data' => $sales]);

            $sales = TicketSale::with('user','payment','items')->withSum('items', TicketSaleItem::QTY);
            return Datatables::eloquent($sales)
                ->addIndexColumn()
                ->addColumn('name', function (TicketSale $sale) {
                    return '<p class="mb-0">'.$sale->cust_name.'</p><small>'.$sale->user->name.'</small>';
                })
                ->addColumn('total', function(TicketSale $sale) {
                    return '<p class="mb-1">Rp. '.number_format($sale->total_price, 0, '', '.').'</p><small>- Rp. '.number_format($sale->discount, 0, '', '.').'</small>';
                })
                ->addColumn('final', function(TicketSale $sale) {
                    return '<h6 class="mb-0"><b>Rp. '.number_format($sale->final_amount, 0, '', '.').'</b></h6><small>'.$sale->id.'</small>';
                })
                ->addColumn('paid', function(TicketSale $sale) {
                    return '<p class="mb-0">Rp. '.number_format($sale->paid_amount, 0, '', '.').'</p><small> Rp. '.number_format($sale->change, 0, '', '.').'</small>';
                })
                ->addColumn('sum_qty', function($row) {
                    return $row->items_sum_qty.' Tiket';
                })
                ->addColumn('tickets', function($row){
                    $html = '<small>';
                    foreach ($row->items as $key => $value) {
                        $html .= $value->qty.' x '.$value->name.'<br>';
                    };
                    return $html .= '</small>';
                })
                ->addColumn('status', function(TicketSale $sale) {
                    if ($sale->is_completed)
                        return '<span class="badge badge-sm badge-success"> Selesai </span>';
                    else 
                        return '<span class="badge badge-sm badge-secondary"> Pending </span>';
                })
                ->rawColumns(["name", 'total', 'final', 'paid','tickets', 'status'])
                ->blacklist(['DT_RowIndex','status'])
                ->toJson();
        }

        return view('ticket-sale.index');
    }
}
