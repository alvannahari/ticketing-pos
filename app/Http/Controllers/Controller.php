<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleActivityRequest;
use App\Models\LogActivity;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;

class Controller extends BaseController {

    use AuthorizesRequests, ValidatesRequests;

    function response_success($message, $data) {
        return response()->json([
            'status' => true,
            'message' => $message,
            'data' => $data,
        ], 200);
    }

    function response_error($message, $error, $code) {
        return response()->json([
            'status' => false,
            'message' => $message,
            'errors' => $error,
        ], $code);
    }

    function save_activity($log_name, $data = null, $desc = null) {
        LogActivity::create([
            LogActivity::USER_ID => auth()->id() ?? request()->user(),
            LogActivity::LOG_NAME => $log_name,
            LogActivity::DEVICE => $data[LogActivity::DEVICE] ?? auth()->user()->getRoleNames()->first(),
            LogActivity::ROLE => $data[LogActivity::ROLE] ?? request()->user()->getRoleNames()->first(),
            LogActivity::DATA => json_encode($data != null ? $data['data'] : null),
            LogActivity::DESC => $desc,
        ]);
    }

    function customer_display($type, $name = null, $price = 0) {
        // $port = config('database.display.port');

        // $con = exec("mode ".$port);
        // if ($con != '') return false;

        // exec("mode ".$port." BAUD=9600 PARITY=n DATA=8 STOP=1 to=off dtr=on rts=on");
        
        // $fp = dio_open($port, O_RDWR);

        // if (!$fp) 
        //     return false;

        // if ($type == 'new_line') {
        //     dio_write($fp, "                    ");
        // } else {
        //     if($type == 'home') {
        //         $line1 = "  Museum & Galeri   ";
        //         $line2 = "      SBY*ANI       ";
        //     } else {
        //         $line1 = $name;
        //         $line2 = "Rp. ". number_format($price,0,'','.');
    
        //         if (strlen($line1) < 20) $line1 = str_pad($line1, 20, ' ', STR_PAD_RIGHT);
        //         else if (strlen($line1) > 20) $line1 = substr($line1,0,17)."...";
    
        //         if (strlen($line2) < 20) $line2 = str_pad($line2, 20, ' ', STR_PAD_LEFT);
        //         else if (strlen($line2) > 20) $line2 = substr($line2,0,17)."...";
        //     }

        //     dio_write($fp, $line1);
        //     dio_write($fp, $line2);
        // }
        
        // dio_close($fp);

        // ------------------ till the end

        // phpinfo();
        // exec("type con>com2 asdasdasd");

        // $output = exec("mode COM2: BAUD=115200 PARITY=N data=8 stop=1 XON=off TO=on dtr=off odsr=off octs=off rts=on idsr=off");
        // $fp = fopen("COM2", "r+");
        // if (!$fp)
        // {
        // exit("Unable to establish a connection");
        // }
        // // RX form PC**************
        // $t = "Test Halo dek";
        // // TX to Arduino****************
        // $writtenBytes = fputs($fp, $t);
        // sleep(1); 
        // // RX from Arduino**************
        // $j=0;
        // $dataset1 = [];
        // while(!$buffer=stream_get_line($fp,400,"\n")) { 1; }
        // // TX to PC***************
        // $piecesa = explode(",", $buffer);
        // foreach ($piecesa as $value) {  
        //     $dataset1[$j] = $value;
        //     $j++;
        // }
        // $myJSON = json_encode($dataset1);
        // echo $myJSON;
        // fclose($fp);

        // $val= "123468";
        // exec("mode com2: BAUD=9600 PARITY=n DATA=8 STOP=1 to=off dtr=on rts=on");
        // $fp = fopen("COM2", "w");
        // //$fp = fopen(‘/dev/ttyUSB0′,’r+’); //you can use this for Linux
        // fwrite($fp, "\f");
        // fwrite($fp,$val); //write string to serial
        // fclose($fp);

        // $fp = fopen ("com2", "w+");
        // $string ='';
        // if (!$fp) {
        //     echo 'not open';
        // }
        // else{
        //     echo 'port is open for write<br/>';
        //     $string .= '<STX>C30C10178C10100C103110606C103081000C10100C10101C100<ETX>';
        //     fputs ($fp, $string );
        //     echo $string;
        //     fclose ($fp);
        // }

        // $serialPort = new SerialPort(new SeparatorParser(), new TTYConfigure());

        // $serialPort->open("COM2");
        // while ($data = $serialPort->read()) {
        //     echo $data."\n";

        //     $serialPort->write("1\n");
        //     $serialPort->close();
        //     if ($data === "OK") {
        //     }
        // }

        // $fp = fopen('COM2', 'r+');

        // if (!$fp) {
        //     echo "Port not accessible";
        // } else {
        //     $writtenBytes = fputs($fp, "tttttt");
        //     echo "Bytes written to port: ".$writtenBytes;
        //     fclose($fp);
        // }

        // Let's start the class
        // $serial = new PhpSerial;

        // // First we must specify the device. This works on both linux and windows (if
        // // your linux serial device is /dev/ttyS0 for COM1, etc)
        // $serial->deviceSet("COM2");

        // // We can change the baud rate, parity, length, stop bits, flow control
        // $serial->confBaudRate(9600);
        // $serial->confParity("none");
        // $serial->confCharacterLength(8);
        // $serial->confStopBits(1);
        // $serial->confFlowControl("none");

        // // Then we need to open it
        // $serial->deviceOpen();

        // // To write into
        // $serial->sendMessage("Helloddd !");
    }
}
