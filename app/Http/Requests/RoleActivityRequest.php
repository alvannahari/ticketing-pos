<?php

namespace App\Http\Requests;

use App\Models\LogActivity;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class RoleActivityRequest extends FormRequest {

    public function authorize(): bool {
        return true;
    }

    public function rules(): array {
        return [
            LogActivity::DEVICE => 'required|string',
            LogActivity::ROLE => 'nullable',
            LogActivity::DATA => 'nullable',
            LogActivity::DESC => 'nullable',
        ];
    }

    protected function failedValidation(Validator $validator) {
        $response = response()->json([
            'status' => false,
            'message' => 'Terjadi kesalahan permintaan.',
            'errors' => $validator->errors()
        ]);            
        // if($this->wantsJson()) {
        //     $response = response()->json([
        //         'success' => false,
        //         'message' => 'Ops! Some errors occurred',
        //         'errors' => $validator->errors()
        //     ]);            
        // }else{
        //     $response = redirect()
        //         ->route('guest.login')
        //         ->with('message', 'Ops! Some errors occurred')
        //         ->withErrors($validator);
        // }
        
        throw (new ValidationException($validator, $response))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }
}
