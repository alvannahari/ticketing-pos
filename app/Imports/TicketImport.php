<?php

namespace App\Imports;

use App\Models\Ticket;
use App\Models\TicketType;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;

class TicketImport implements ToCollection, WithHeadingRow {

    function collection(Collection $rows) {
        $count_free = 0;
        $count_domestic = 0;
        $count_national = 0;
        $count_international = 0;

        foreach ($rows as $row) {
            if ($row['gratis'] != null) {
                Ticket::create([
                    Ticket::BARCODE     => $row['gratis'],
                    Ticket::TYPE_ID    => 1,
                ]);
                $count_free++;
            }
            if ($row['domestik'] != null) {
                Ticket::create([
                    Ticket::BARCODE     => $row['domestik'],
                    Ticket::TYPE_ID    => 2,
                ]);
                $count_domestic++;
            }
            if ($row['nasional'] != null) {
                Ticket::create([
                    Ticket::BARCODE     => $row['nasional'],
                    Ticket::TYPE_ID    => 3,
                ]);
                $count_national++;
            }
            if ($row['internasional'] != null) {
                Ticket::create([
                    Ticket::BARCODE     => $row['internasional'],
                    Ticket::TYPE_ID    => 4,
                ]);
                $count_international++;
            }
        }

        if ($count_free > 0) {
            $inventory = TicketType::find(TicketType::TICKET_FREE)->inventory;
            TicketType::find(TicketType::TICKET_FREE)->update([TicketType::INVENTORY => ((int) $inventory + $count_free)]);
        }
        if ($count_domestic > 0) {
            $inventory = TicketType::find(TicketType::TICKET_DOMESTIC)->inventory;
            TicketType::find(TicketType::TICKET_DOMESTIC)->update([TicketType::INVENTORY => ((int) $inventory + $count_domestic)]);
        }
        if ($count_national > 0) {
            $inventory = TicketType::find(TicketType::TICKET_NATIONAL)->inventory;
            TicketType::find(TicketType::TICKET_NATIONAL)->update([TicketType::INVENTORY => ((int) $inventory + $count_national)]);
        }
        if ($count_international > 0) {
            $inventory = TicketType::find(TicketType::TICKET_INTERNATIONAL)->inventory;
            TicketType::find(TicketType::TICKET_INTERNATIONAL)->update([TicketType::INVENTORY => ((int) $inventory + $count_international)]);
        }
    }
}
