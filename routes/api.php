<?php

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\Ticket\TicketController;
use App\Http\Controllers\Api\V1\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/', function () { 
    return response()->json([
        'status' => true,
        'version' => 'Api Ticketing POS v.0.1'
    ]);
})->name('api.version');

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', [AuthController::class, 'login'])->withoutMiddleware('auth:sanctum');
        Route::post('role_activity', [AuthController::class, 'role_activity']);
        Route::post('logout', [AuthController::class, 'logout']);
    });

    Route::group(['prefix' => 'ticket'], function () {
        Route::get('print', [TicketController::class, 'print']);
        Route::get('get_type_ticket', [TicketController::class, 'get_type_ticket']);
        Route::get('get_type_payment', [TicketController::class, 'get_type_payment']);
        Route::post('transaction', [TicketController::class, 'store_transaction']);
        Route::get('list_transaction_history', [TicketController::class, 'list_transaction_history']);
        Route::get('detail_transaction', [TicketController::class, 'detail_transaction']);
        Route::post('scan_barcode', [TicketController::class, 'scan_barcode']);
        Route::get('list_entrance_history', [TicketController::class, 'list_entrance_history']);
    });
});

Route::post('synchronized-data', [TransactionController::class, 'synchronized_data']);


