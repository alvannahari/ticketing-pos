<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\LogActivityController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductSaleController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\TicketSaleController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::group(['namespace' => 'Auth'], function() {
    Route::get('login', [LoginController::class,'showLoginForm'])->name('login');
    Route::post('login', [LoginController::class,'login'])->name('login.store');
    Route::post('logout', [LoginController::class,'logout'])->name('logout')->middleware('auth');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function() { return redirect()->route('dashboard'); });
    Route::get('dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');
    Route::get('dashboard-ticket', [DashboardController::class, 'ticket'])->name('dashboard.ticket');
    Route::get('dashboard-store', [DashboardController::class, 'store'])->name('dashboard.store');
    
    Route::resource('tickets', TicketController::class)->only(['destroy']);
    Route::get('tickets-valid', [TicketController::class, 'valid'])->name('ticket.valid');
    Route::get('tickets-invalid', [TicketController::class, 'invalid'])->name('ticket.invalid');
    Route::post('ticket-import', [TicketController::class, 'import'])->name('ticket.import');

    Route::resource('ticket-sales', TicketSaleController::class)->only(['index','destroy']);

    Route::resource('products', ProductController::class)->except(['edit']);
    Route::get('product-barcode/{code}', [ProductController::class, 'print_barcode'])->name('product.print-barcode');
    Route::post('export/products', [ProductController::class, 'export'])->name('export.products');

    Route::resource('product-sales', ProductSaleController::class)->only(['index', 'show', 'destroy']);

    Route::get('transaction/synchronized-data', [LogActivityController::class, 'synchronized_data'])->name('transaction.synchronized-data');
    Route::resource('transaction', TransactionController::class)->only(['index', 'store', 'show']);
    
    Route::resource('users', UserController::class)->only(['index', 'store', 'destroy']);
    Route::post('user/change-role/{user}', [UserController::class, 'changeRole'])->name('user.change-role');
    Route::post('user/change-password/{user}', [UserController::class, 'changePassword'])->name('user.change-password');
    
    Route::get('report-tickets', [ReportController::class, 'reportTickets'])->name('report.tickets');
    Route::post('report-tickets/export', [ReportController::class, 'exportTickets'])->name('report.export.tickets');
    Route::get('report-store', [ReportController::class, 'reportStore'])->name('report.store');
    Route::post('report-store/export', [ReportController::class, 'exportStore'])->name('report.export.store');

    Route::resource('log-activities', LogActivityController::class)->only(['index', 'destroy']);
});

Route::get('customer-display/{type}', [Controller::class, 'customer_display'])->name('customer.display');
Route::get('product-display/{name}/{price}', [ProductController::class, 'display_product'])->name('product.display');